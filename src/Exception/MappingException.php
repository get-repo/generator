<?php

namespace GetRepo\Generator\Exception;

class MappingException extends \Exception implements GeneratorException
{
}
