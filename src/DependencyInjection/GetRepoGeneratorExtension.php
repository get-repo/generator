<?php

namespace GetRepo\Generator\DependencyInjection;

use GetRepo\Generator\Configuration\GeneratorConfiguration;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\Workflow\Registry as WorkflowRegistry;

class GetRepoGeneratorExtension extends Extension implements PrependExtensionInterface
{
    final public const ALIAS = 'getrepo_generator';

    public function getAlias(): string
    {
        return self::ALIAS;
    }

    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new GeneratorConfiguration();
        $config = $this->processConfiguration($configuration, $configs);
        // load yaml files
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../../config'));
        $loader->load('services.yml');
        $loader->load('formyaml.yml');
        $container->setParameter(self::ALIAS . '.config', $config);
    }

    public function prepend(ContainerBuilder $container)
    {
        if (!$container->hasExtension('framework')) {
            // set empty workflow
            $definition = new Definition(WorkflowRegistry::class);
            $container->setDefinition(WorkflowRegistry::class, $definition);
        }
    }
}
