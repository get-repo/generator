<?php

namespace GetRepo\Generator\Util;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\String\ByteString;

class MappingUtil
{
    public const FIELDS = 'fields';
    public const ONE_TO_ONE = 'oneToOne';
    public const ONE_TO_MANY = 'oneToMany';
    public const MANY_TO_ONE = 'manyToOne';
    public const MANY_TO_MANY = 'manyToMany';
    public const ALL_FIELDS = [
        self::FIELDS,
        self::ONE_TO_ONE,
        self::ONE_TO_MANY,
        self::MANY_TO_ONE,
        self::MANY_TO_MANY,
    ];

    public static function hasFieldOrAssoc(array $mapping): bool
    {
        foreach (self::ALL_FIELDS as $key) {
            if (isset($mapping[$key]) && $mapping[$key]) {
                return true;
            }
        }

        return false;
    }

    public static function getAllFieldsAndAssocs(array $mapping): array
    {
        $all = array_merge(self::getAllFields($mapping), self::getAllAssocs($mapping));

        self::sort($all);

        return $all;
    }

    public static function getAllFields(array $mapping): array
    {
        $fields = $mapping[self::FIELDS] ?? [];

        self::sort($fields);

        return $fields;
    }

    public static function getAllAssocs(array $mapping, bool $splitted = false): array
    {
        $assocs = self::ALL_FIELDS;
        unset($assocs[array_search(self::FIELDS, self::ALL_FIELDS)]);
        $all = [];
        foreach ($assocs as $key) {
            if (isset($mapping[$key]) && $mapping[$key]) {
                if ($splitted) {
                    $all[$key] = array_merge($all[$key] ?? [], $mapping[$key]);
                } else {
                    $all = array_merge($all, $mapping[$key]);
                }
            }
        }

        if (!$splitted) {
            self::sort($all);
        }

        return $all;
    }

    public static function getPropertyMatches(array $array, string $class): array
    {
        $rClass = new \ReflectionClass($class);
        $matches = [];
        foreach ($array as $k => $v) {
            $k = (new ByteString($k))->camel()->toString();
            if ($rClass->hasProperty($k)) {
                $rProperty = $rClass->getProperty($k);
                if (!$rProperty->hasDefaultValue() || $v !== $rProperty->getDefaultValue()) {
                    $matches[$k] = $v;
                }
            }
        }

        return $matches;
    }

    public static function getTypeHint(?string $type): string
    {
        return match ($type) {
            'boolean' => 'bool',
            'integer', 'bigint' => 'int',
            'float', 'decimal' => 'float',
            'simple_array', 'json', 'array' => 'array',
            'date', 'datetime' => \DateTime::class,
            'date_immutable', 'datetime_immutable' => \DateTimeImmutable::class,
            default => 'string',
        };
    }

    public static function getAssocClass(string $assocType): string
    {
        return match ($assocType) {
            self::ONE_TO_ONE => ORM\OneToOne::class,
            self::ONE_TO_MANY => ORM\OneToMany::class,
            self::MANY_TO_ONE => ORM\ManyToOne::class,
            self::MANY_TO_MANY => ORM\ManyToMany::class,
            default => throw new \RuntimeException("Relationship \"{$assocType}\" is not implemented yet."),
        };
    }

    public static function sort(array &$array): void
    {
        // TODO get we also sort optional relationship at the end ? #sort-optional-assoc-end
        // sort by nullable or default value (push them at the end of array)
        uasort($array, function (?array $field) {
            return ($field['nullable'] ?? false) || (array_key_exists('default', $field['options'] ?? [])) ? 1 : -1;
        });
    }
}
