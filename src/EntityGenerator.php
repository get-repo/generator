<?php

namespace GetRepo\Generator;

use GetRepo\Generator\Configuration\EntityConfiguration;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Contracts\Service\Attribute\Required;

class EntityGenerator extends AbstractGenerator
{
    private EntityConfiguration $entityConfiguration;

    #[Required]
    public function getConfiguration(EntityConfiguration $entityConfiguration): void
    {
        $this->entityConfiguration = $entityConfiguration;
    }

    protected function buildConfiguration(): ConfigurationInterface
    {
        return $this->entityConfiguration;
    }
}
