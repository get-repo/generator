<?php

namespace GetRepo\Generator;

use GetRepo\Generator\Configuration\ControllerConfiguration;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Contracts\Service\Attribute\Required;

class ControllerGenerator extends AbstractGenerator
{
    private ControllerConfiguration $controllerConfiguration;

    #[Required]
    public function getConfiguration(ControllerConfiguration $controllerConfiguration): void
    {
        $this->controllerConfiguration = $controllerConfiguration;
    }

    protected function buildConfiguration(): ConfigurationInterface
    {
        return $this->controllerConfiguration;
    }
}
