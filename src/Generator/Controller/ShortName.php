<?php

namespace GetRepo\Generator\Generator\Controller;

use GetRepo\Generator\Generator\AbstractComposition;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;

class ShortName extends AbstractComposition
{
    public static function getPriority(): int
    {
        return 100;
    }

    public static function supports(array $mapping, array $compositionConfs): bool
    {
        return true;
    }

    public function __invoke(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        array $compositionConfs,
    ): void {
        $shortName = $classGenerator->getName();
        $prefix = $compositionConfs['prefix'];
        if (count($compositionConfs['inheritance']) > 0) {
            $inheritance = $compositionConfs['inheritance'];
            $shortName = sprintf(
                '%s%s',
                $compositionConfs['inheritance'][$namespaceGenerator->getName()]['prefix'] ?? $prefix,
                $classGenerator->getName(),
            );
            if (array_key_last($inheritance) !== $namespaceGenerator->getName()) {
                $shortName = sprintf('Abstract%s', $shortName);
            }
        } elseif ($prefix) {
            $shortName = sprintf('%s%s', $prefix, $shortName);
        }
        // add controller suffix all the time
        $classGenerator->setName(sprintf('%sController', $shortName));
    }
}
