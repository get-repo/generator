<?php

namespace GetRepo\Generator\Generator\Controller;

use GetRepo\Generator\Generator\AbstractComposition;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;

class Abstraction extends AbstractComposition
{
    public static function getPriority(): int
    {
        return 90;
    }

    public static function supports(array $mapping, array $compositionConfs): bool
    {
        /** @see ShortName::supports() */
        return count($compositionConfs['inheritance']) > 0;
    }

    public function __invoke(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        array $compositionConfs,
    ): void {
        /** @see ShortName::__invoke() */
        $inheritance = $compositionConfs['inheritance'];
        $namespace = $namespaceGenerator->getName();
        if (array_key_last($inheritance) !== $namespace) {
            $classGenerator->setAbstract();
        }
    }
}
