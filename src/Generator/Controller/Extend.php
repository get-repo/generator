<?php

namespace GetRepo\Generator\Generator\Controller;

use GetRepo\Generator\Generator\AbstractComposition;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;

class Extend extends AbstractComposition
{
    public static function getPriority(): int
    {
        return Abstraction::getPriority() + 1;
    }

    public static function supports(array $mapping, array $compositionConfs): bool
    {
        return count($compositionConfs['inheritance']) > 0 || isset($compositionConfs['extends']);
    }

    public function __invoke(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        array $compositionConfs,
    ): void {
        if ($compositionConfs['generated'] ?? false) {
            /** @var \GetRepo\Generator\Generator\Generated $extends */
            $extends = end($compositionConfs['generated']);
            $extendsClassGenerator = $extends->getClassGenerator();
            $extendsNamespaceGenerator = $extends->getNamespaceGenerator();
            $extendsClassName = sprintf(
                '%s\%s',
                $extendsNamespaceGenerator->getName(),
                $extendsClassGenerator->getName(),
            );
            $duplicates = $extendsClassGenerator->getName() === $classGenerator->getName();
            $namespaceGenerator->addUse($extendsClassName, $duplicates ? "Base{$classGenerator->getName()}" : null);
            $classGenerator->setExtends($extendsClassName);
        } elseif ($compositionConfs['extends']) {
            $namespaceGenerator->addUse($compositionConfs['extends']);
            $classGenerator->setExtends($compositionConfs['extends']);
        }
    }
}
