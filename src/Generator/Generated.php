<?php

namespace GetRepo\Generator\Generator;

use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;
use Nette\PhpGenerator\Printer;

class Generated
{
    public function __construct(
        private PhpNamespace $namespaceGenerator,
        private ClassType $classGenerator,
    ) {
    }

    public function getFullClassname(): string
    {
        return sprintf(
            '%s\%s',
            $this->namespaceGenerator->getName(),
            $this->classGenerator->getName(),
        );
    }

    public function getNamespaceGenerator(): PhpNamespace
    {
        return $this->namespaceGenerator;
    }

    public function getClassGenerator(): ClassType
    {
        return $this->classGenerator;
    }

    public function print(Printer $printer): string
    {
        return "<?php\n\n" . $printer->printNamespace($this->namespaceGenerator);
    }
}
