<?php

namespace GetRepo\Generator\Generator\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use GetRepo\Generator\Generator\AbstractComposition;
use GetRepo\Generator\Util\MappingUtil;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;

class Identifier extends AbstractComposition
{
    public static function getPriority(): int
    {
        return Constructor::getPriority() - 1;
    }

    public static function supports(array $mapping, array $compositionConfs): bool
    {
        return $compositionConfs['identifiers'] && count($mapping['identifiers']) > 0;
    }

    public function __invoke(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        array $compositionConfs,
    ): void {
        if (!isset($compositionConfs['generated'])) {
            foreach ($mapping['identifiers'] as $name => $identifierMapping) {
                $property = $classGenerator->addProperty($name);
                $namespaceGenerator->addUse(Id::class);
                $property->addAttribute(Id::class);
                $namespaceGenerator->addUse(GeneratedValue::class);
                $property->addAttribute(
                    GeneratedValue::class,
                    ['strategy' => $identifierMapping['generator']['strategy']],
                );
                $namespaceGenerator->addUse(Column::class);
                $property->addAttribute(Column::class);
                $property->setProtected();
                $property->setNullable();
                $property->setValue(null);
                $property->setType(MappingUtil::getTypeHint($identifierMapping['type']));

                $getter = $classGenerator->addMethod('get' . ucfirst($name));
                $getter->setReturnType($property->getType());
                $getter->setReturnNullable();
                $getter->setBody(sprintf('return $this->%s;', $name));
            }
        }
    }
}
