<?php

namespace GetRepo\Generator\Generator\Entity;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Gedmo\Sortable\Entity\Repository\SortableRepository;
use GetRepo\DoctrineExtension\OneRow\Service\OneRowRepository;
use GetRepo\Generator\Generator\AbstractComposition;
use GetRepo\Generator\Util\MappingUtil;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\Literal;
use Nette\PhpGenerator\PhpNamespace;

class Doctrine extends AbstractComposition
{
    public static function getPriority(): int
    {
        return Properties::getPriority() - 1;
    }

    public static function supports(array $mapping, array $compositionConfs): bool
    {
        // TODO duplicate check no inheritance or is last #check_no_inheritance_or_is_last
        return (
            !count($compositionConfs['inheritance']) // no inheritance
            ||
            true === ($compositionConfs['inheritance_is_last'] ?? false) // last entity
        );
    }

    public function __invoke(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        array $compositionConfs,
    ): void {
        if (!isset($mapping['discriminator'])) {
            $entityAttributeOptions = [];
            // sortable repository
            if ($mapping['behaviors']['sortable'] ?? false) {
                $namespaceGenerator->addUse(SortableRepository::class);
                $entityAttributeOptions['repositoryClass'] = new Literal('SortableRepository::class');
            }
            // onerow repository
            if ($mapping['behaviors']['onerow'] ?? false) {
                $namespaceGenerator->addUse(OneRowRepository::class);
                $entityAttributeOptions['repositoryClass'] = new Literal('OneRowRepository::class');
            }
            $namespaceGenerator->addUse(Entity::class);
            $classGenerator->addAttribute(Entity::class, $entityAttributeOptions);

            if (!isset($mapping['isDiscriminatedBy'])) {
                $namespaceGenerator->addUse(Table::class);
                $tableOptions = array_merge(
                    ['name' => ($mapping['table'] ?? null)],
                    ['options' => $mapping['options'] ?? []],
                );
                $classGenerator->addAttribute(
                    Table::class,
                    MappingUtil::getPropertyMatches($tableOptions, Table::class),
                );
            }
        } elseif (true === ($compositionConfs['inheritance_is_last'] ?? false)) {
            $entityAttributeOptions = [];
            // sortable repository
            if ($mapping['behaviors']['sortable'] ?? false) {
                $namespaceGenerator->addUse(SortableRepository::class);
                $entityAttributeOptions['repositoryClass'] = new Literal('SortableRepository::class');
            }
            $namespaceGenerator->addUse(Entity::class);
            $classGenerator->addAttribute(Entity::class, $entityAttributeOptions);
        }
    }
}
