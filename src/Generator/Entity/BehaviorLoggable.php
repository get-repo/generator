<?php

namespace GetRepo\Generator\Generator\Entity;

use Gedmo\Mapping\Annotation\Loggable;
use Gedmo\Mapping\Annotation\Versioned;
use GetRepo\Generator\Generator\AbstractComposition;
use GetRepo\Generator\Util\MappingUtil;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;

class BehaviorLoggable extends AbstractComposition
{
    public static function getPriority(): int
    {
        return 10;
    }

    public static function supports(array $mapping, array $compositionConfs): bool
    {
        return isset($mapping['behaviors']['loggable']); // has loggable behavior
    }

    public function __invoke(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        array $compositionConfs,
    ): void {
        if (!isset($compositionConfs['generated'])) {
            $loggable = $mapping['behaviors']['loggable'];
            if (!isset($mapping['discriminator'])) {
                $versioned = [];
                $loggable['blacklist'] = \array_merge(
                    ['createdAt', 'updatedAt', 'createdBy', 'updatedBy'],
                    $loggable['blacklist'],
                );
                $allFields = array_keys(MappingUtil::getAllFieldsAndAssocs($mapping));
                // sort to have whitelist / blacklist key order
                krsort($loggable);
                // apply whitelist / blacklist
                foreach ($loggable as $list => $fields) {
                    // true = all fields
                    if (true === $fields) {
                        $fields = $allFields;
                    }
                    $fields = (array) $fields;

                    switch ($list) {
                        case 'whitelist':
                            $versioned = array_merge($versioned, $fields);
                            break;

                        case 'blacklist':
                            foreach ($fields as $name) {
                                if (false !== ($key = array_search($name, $versioned))) {
                                    unset($versioned[$key]);
                                }
                            }
                            break;
                    }
                }

                // unique versioned fields
                $versioned = array_values(array_unique($versioned));

                if ($versioned) {
                    $namespaceGenerator->addUse(Versioned::class);
                    foreach ($versioned as $field) {
                        $property = null;
                        // TODO duplicate code #properties_or_constructor_promotion_field
                        if ($classGenerator->hasProperty($field)) {
                            $property = $classGenerator->getProperty($field); // throw exception
                        } else {
                            $property = Constructor::getConstructor(classGenerator: $classGenerator)
                                ->getParameter($field); // throw exception
                        }
                        $property->addAttribute(Versioned::class);
                    }
                }
            }
        } elseif (true === ($compositionConfs['inheritance_is_last'] ?? false)) {
            $namespaceGenerator->addUse(Loggable::class);
            // TODO logEntryClass in config
            $classGenerator->addAttribute(Loggable::class, ['logEntryClass' => 'App\Entity\ActivityLog']);
        }
    }
}
