<?php

namespace GetRepo\Generator\Generator\Entity;

use Doctrine\ORM\Mapping\Column;
use Gedmo\Mapping\Annotation\SortableGroup;
use Gedmo\Mapping\Annotation\SortablePosition;
use GetRepo\Generator\Configuration\EntityConfiguration;
use GetRepo\Generator\Generator\AbstractComposition;
use GetRepo\Generator\Util\MappingUtil;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;

class BehaviorSortable extends AbstractComposition
{
    public static function getPriority(): int
    {
        return 10;
    }

    public static function supports(array $mapping, array $compositionConfs): bool
    {
        return !isset($compositionConfs['generated']) // only for main class
            && isset($mapping['behaviors']['sortable']); // has sortable behavior
    }

    public function __invoke(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        array $compositionConfs,
    ): void {
        $sortable = $mapping['behaviors']['sortable'];
        $property = $classGenerator->addProperty(EntityConfiguration::SORTABLE_FIELD);
        $namespaceGenerator->addUse(Column::class);
        $property->addAttribute(
            Column::class,
            MappingUtil::getPropertyMatches($mapping['fields'][EntityConfiguration::SORTABLE_FIELD], Column::class),
        );
        $property->setVisibility($compositionConfs['properties']['visibility'] ?? 'protected');
        $property->setType(MappingUtil::getTypeHint('integer'));
        $namespaceGenerator->addUse(SortablePosition::class);
        $property->addAttribute(SortablePosition::class);
        if ($sortable['grouping']) {
            $namespaceGenerator->addUse(SortableGroup::class);
            foreach ($sortable['grouping'] as $field) {
                // TODO duplicate code #properties_or_constructor_promotion_field
                if ('constructor_property_promotion' !== ($compositionConfs['properties']['define_as'] ?? null)) {
                    $property = $classGenerator->getProperty($field);
                } else {
                    $property = Constructor::getConstructor(classGenerator: $classGenerator)->getParameter($field);
                }
                $property->addAttribute(SortableGroup::class);
            }
        }
    }
}
