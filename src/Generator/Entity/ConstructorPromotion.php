<?php

namespace GetRepo\Generator\Generator\Entity;

use GetRepo\Generator\Generator\AbstractComposition;
use GetRepo\Generator\Util\MappingUtil;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;
use Nette\PhpGenerator\PromotedParameter;

class ConstructorPromotion extends AbstractComposition
{
    public static function getPriority(): int
    {
        return min(Constructor::getPriority(), Properties::getPriority()) - 5;
    }

    public static function supports(array $mapping, array $compositionConfs): bool
    {
        return 'constructor_property_promotion' === ($compositionConfs['properties']['define_as'] ?? null)
            && !isset($mapping['isDiscriminatedBy'])
            && MappingUtil::hasFieldOrAssoc($mapping);
    }

    public function __invoke(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        array $compositionConfs,
    ): void {
        if (!isset($compositionConfs['generated'])) {
            $constructor = Constructor::getConstructor(classGenerator: $classGenerator);
            $this->generateFieldsAndAssoc(
                classGenerator: $classGenerator,
                namespaceGenerator: $namespaceGenerator,
                mapping: $mapping,
                visibility: $compositionConfs['properties']['visibility'],
                parameterBuilder: function (string $name) use ($constructor): PromotedParameter {
                    return $constructor->addPromotedParameter($name);
                },
            );
        }
    }
}
