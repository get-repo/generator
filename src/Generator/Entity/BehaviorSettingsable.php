<?php

namespace GetRepo\Generator\Generator\Entity;

use Gedmo\Mapping\Settingsable;
use GetRepo\DoctrineExtension\Settingsable\Service\SettingsableTrait;
use GetRepo\Generator\Generator\AbstractComposition;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;

class BehaviorSettingsable extends AbstractComposition
{
    public static function getPriority(): int
    {
        return Identifier::getPriority() + 1;
    }

    public static function supports(array $mapping, array $compositionConfs): bool
    {
        // only for main class
        return !isset($compositionConfs['generated']) && isset($mapping['behaviors']['settingsable']);
    }

    public function __invoke(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        array $compositionConfs,
    ): void {
        if (isset($mapping['discriminator']) || !isset($mapping['isDiscriminatedBy'])) {
            $namespaceGenerator->addUse(SettingsableTrait::class);
            $classGenerator->addTrait(SettingsableTrait::class);
        }

        $method = $classGenerator->addMethod(Settingsable::DEFAULT_METHOD);
        $method->setStatic();
        $method->setReturnType('array');
        if ($config = $mapping['behaviors']['settingsable']['config']) {
            // prepare nice array code
            foreach ($config as $k => &$v) {
                unset($v[($v['mandatory'] ? 'default' : 'mandatory')]);
                foreach (['allowed_types', 'allowed_values'] as $key) {
                    if (isset($v[$key]) && !$v[$key]) {
                        unset($v[$key]);
                    }
                }
            }
            $method->setBody(sprintf('return %s;', $this->varExportArray($config)));
        } else {
            $method->setBody('return [];');
        }
    }

    private function varExportArray(array $array): string
    {
        $export = var_export($array, true);
        $export = preg_replace("/^([ ]*)(.*)/m", '$1$1$2', $export);
        $array = preg_split("/\r\n|\n|\r/", $export);
        $array = preg_replace(["/\s*array\s\($/", "/\)(,)?$/", "/\s=>\s$/"], [null, ']$1', ' => ['], $array);
        $export = join(PHP_EOL, array_filter(["["] + $array));

        return $export;
    }
}
