<?php

namespace GetRepo\Generator\Generator\Entity;

use GetRepo\Generator\Generator\AbstractComposition;
use GetRepo\Generator\Util\MappingUtil;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\Parameter;
use Nette\PhpGenerator\PhpNamespace;

class ConstructorPropertyArgs extends AbstractComposition
{
    public static function getPriority(): int
    {
        return Constructor::getPriority() + 1;
    }

    public static function supports(array $mapping, array $compositionConfs): bool
    {
        return 'property_with_constructor_args' === ($compositionConfs['properties']['define_as'] ?? null)
            && !isset($mapping['isDiscriminatedBy'])
            && MappingUtil::hasFieldOrAssoc($mapping);
    }

    public function __invoke(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        array $compositionConfs,
    ): void {
        if (!isset($compositionConfs['generated'])) {
            $constructor = Constructor::getConstructor(classGenerator: $classGenerator);
            $this->generateFieldsAndAssoc(
                classGenerator: $classGenerator,
                namespaceGenerator: $namespaceGenerator,
                mapping: $mapping,
                visibility: $compositionConfs['properties']['visibility'],
                parameterBuilder: function (string $name) use ($constructor): Parameter {
                    $constructor->addBody(sprintf('$this->%1$s = $%1$s;', $name));
                    return $constructor->addParameter($name);
                },
                options: ['doctrine_attributes' => false],
            );
        }
    }
}
