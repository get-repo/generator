<?php

namespace GetRepo\Generator\Generator\Entity;

use Doctrine\ORM\Mapping\Column;
use Gedmo\Mapping\Annotation\Slug;
use GetRepo\Generator\Configuration\EntityConfiguration;
use GetRepo\Generator\Generator\AbstractComposition;
use GetRepo\Generator\Util\MappingUtil;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;

class BehaviorSluggable extends AbstractComposition
{
    public static function getPriority(): int
    {
        return BehaviorLoggable::getPriority() + 1;
    }

    public static function supports(array $mapping, array $compositionConfs): bool
    {
        return !isset($compositionConfs['generated']) // only for main class
            && isset($mapping['behaviors']['sluggable']); // has sluggable behavior
    }

    public function __invoke(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        array $compositionConfs,
    ): void {
        $sluggable = $mapping['behaviors']['sluggable'];
        $property = $classGenerator->addProperty(EntityConfiguration::SLUGGABLE_FIELD);
        $namespaceGenerator->addUse(Column::class);
        $property->addAttribute(
            Column::class,
            MappingUtil::getPropertyMatches($sluggable, Column::class),
        );
        $property->setVisibility($compositionConfs['properties']['visibility'] ?? 'protected');
        $property->setType(MappingUtil::getTypeHint('string'));
        unset($sluggable['form'], $sluggable['validators']);
        $namespaceGenerator->addUse(Slug::class);
        $property->addAttribute(Slug::class, MappingUtil::getPropertyMatches($sluggable, Slug::class));
    }
}
