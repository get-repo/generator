<?php

namespace GetRepo\Generator\Generator\Entity;

use GetRepo\Generator\Configuration\Action;
use GetRepo\Generator\Generator\AbstractComposition;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\String\ByteString;
use Symfony\Component\Yaml\Tag\TaggedValue;

class Methods extends AbstractComposition
{
    public const METHOD_TO_STRING_NAME = '__toString';
    public const METHOD_CLONE_NAME = '__clone';

    public static function getPriority(): int
    {
        return min(Constructor::getPriority(), Doctrine::getPriority()) - 1;
    }

    public static function supports(array $mapping, array $compositionConfs): bool
    {
        return !isset($compositionConfs['generated'])
            && !isset($mapping['isDiscriminatedBy'])
            && count($compositionConfs['methods']) > 0;
    }

    public function __invoke(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        array $compositionConfs,
    ): void {
        // magic methods
        if (isset($compositionConfs['methods']['magic']['to_string'])) {
            $toString = $classGenerator->addMethod(self::METHOD_TO_STRING_NAME);
            $toString->setReturnType('string');
            $toString->setBody($compositionConfs['methods']['magic']['to_string']);
        }
        if (
            isset($compositionConfs['methods']['magic']['clone'])
            || ($compositionConfs['methods']['magic']['auto_clone'] ?? false)
        ) {
            $cloneConfig = $compositionConfs['methods']['magic'];
            $clone = $classGenerator->addMethod(self::METHOD_CLONE_NAME);
            $clone->setReturnType('void');
            if (($cloneConfig['auto_clone'] ?? false)) {
                $body = $condition = $expression = [];
                $expression = null;
                if (count($mapping['identifiers'])) {
                    foreach (array_keys($mapping['identifiers']) as $name) {
                        $condition[] = sprintf('$this->%s', $name);
                        $body[] = sprintf('$this->%s = null;', $name);
                    }
                }
                if ($cloning = $mapping['routing']['actions'][Action::DUPLICATE->value]['cloning'] ?? []) {
                    // check if ExpressionLanguage is used
                    foreach ($cloning as $value) {
                        if ($value instanceof TaggedValue) {
                            $namespaceGenerator->addUse(ExpressionLanguage::class);
                            $expression = sprintf('$el = new ExpressionLanguage();');
                            break;
                        }
                    }
                    // check if ExpressionLanguage is used
                    foreach ($cloning as $property => $value) {
                        if ($relationship = $mapping['oneToMany'][$property] ?? false) {
                            if (!$value) {
                                continue;
                            }
                            $body[] = sprintf('/** @var \%s $v */', $relationship['targetEntity']);
                            $body[] = sprintf('foreach ($this->%s as $v) {', $property);
                            $body[] = '    $v = clone $v;';
                            if ($mappedBy = $relationship['mappedBy'] ?? false) {
                                $body[] = sprintf(
                                    '    $v->set%s($this); // @phpstan-ignore-line',
                                    ucfirst($mappedBy),
                                );
                            }
                            $body[] = '    $this->elements->add($v);';
                            $body[] = '}';
                            continue;
                        } elseif (
                            ($mapping['oneToOne'][$property] ?? false)
                            || ($mapping['manyToOne'][$property] ?? false)
                        ) {
                            if (!$value) {
                                continue;
                            }
                            $body[] = sprintf('$this->%1$s = clone $this->%1$s;', $property);
                            continue;
                        } elseif ($value instanceof TaggedValue) {
                            $value = sprintf(
                                '$el->evaluate(\'%s\', [\'source\' => $this])',
                                $value->getValue(),
                            );
                        } else {
                            $value = var_export($value, true);
                        }
                        $body[] = sprintf('$this->%s = %s;', $property, $value);
                    }
                }

                if ($expression) {
                    array_unshift($body, $expression);
                }

                /** @see https://www.doctrine-project.org/projects/doctrine-orm/en/2.6/cookbook/implementing-wakeup-or-clone.html#safely-implementing-clone */
                if ($condition) {
                    // indent
                    foreach ($body as &$line) {
                        $line = str_repeat(' ', 4) . $line;
                    }
                    array_unshift($body, sprintf('if (%s) {', implode(' && ', $condition)));
                    $body[] = '}';
                }
                array_unshift($body, '// phpcs:disable');
                $body[] = '// phpcs:enable';
                $clone->addBody(implode(PHP_EOL, $body));
            }
            if ($cloneConfig['clone'] ?? false) {
                $clone->addBody($cloneConfig['clone']);
            }
        }
        if ($methods = $compositionConfs['methods']['custom']) {
            foreach ($methods as $name => $methodConf) {
                $method = $classGenerator->addMethod($name);
                foreach ($methodConf as $k => $v) {
                    $k = sprintf('set%s', (new ByteString($k))->camel()->title());
                    if (!is_array($v)) {
                        $v = [$v];
                    }
                    $method->{$k}(...$v);
                }
            }
        }
        if ($compositionConfs['methods']['auto_sorting']) {
            // order is important
            $sorted = ['abstract' => [], 'public' => [], 'protected' => [], 'private' => []];
            foreach ($classGenerator->getMethods() as $name => $method) {
                $key = match (true) {
                    $method->isAbstract() => 'abstract',
                    $method->isProtected() => 'protected',
                    $method->isPrivate() => 'private',
                    default => 'public',
                };
                $sorted[$key][$name] = $method;
            }
            $methods = [];
            foreach ($sorted as $type => $typedMethods) {
                $methods = array_merge($methods, $typedMethods);
            }
            $classGenerator->setMethods($methods);
        }
    }
}
