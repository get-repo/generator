<?php

namespace GetRepo\Generator\Generator\Entity;

use GetRepo\Generator\Generator\AbstractComposition;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;

class ShortName extends AbstractComposition
{
    public static function getPriority(): int
    {
        return 100;
    }

    public static function supports(array $mapping, array $compositionConfs): bool
    {
        return Abstraction::supports($mapping, $compositionConfs);
    }

    public function __invoke(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        array $compositionConfs,
    ): void {
        $inheritance = $compositionConfs['inheritance'];
        $shortName = sprintf(
            '%s%s',
            $compositionConfs['inheritance'][$namespaceGenerator->getName()]['prefix'] ?? $compositionConfs['prefix'],
            $classGenerator->getName(),
        );
        if (array_key_last($inheritance) !== $namespaceGenerator->getName()) {
            $shortName = sprintf('Abstract%s', $shortName);
        }
        $classGenerator->setName($shortName);
    }
}
