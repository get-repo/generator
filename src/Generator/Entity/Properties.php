<?php

namespace GetRepo\Generator\Generator\Entity;

use GetRepo\Generator\Generator\AbstractComposition;
use GetRepo\Generator\Util\MappingUtil;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;

class Properties extends AbstractComposition
{
    public static function getPriority(): int
    {
        return 60;
    }

    public static function supports(array $mapping, array $compositionConfs): bool
    {
        return isset($mapping['isDiscriminatedBy'])
            ||
            (
                'constructor_property_promotion' !== ($compositionConfs['properties']['define_as'] ?? null)
                && isset($compositionConfs['properties'])
                && MappingUtil::hasFieldOrAssoc($mapping)
            );
    }

    public function __invoke(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        array $compositionConfs,
    ): void {
        if (!isset($compositionConfs['generated'])) {
            $this->generateFieldsAndAssoc(
                classGenerator: $classGenerator,
                namespaceGenerator: $namespaceGenerator,
                mapping: $mapping,
                visibility: $compositionConfs['properties']['visibility'] ?? 'protected',
                parameterBuilder: function (string $name) use ($classGenerator) {
                    return $classGenerator->addProperty($name);
                },
            );
        }
    }
}
