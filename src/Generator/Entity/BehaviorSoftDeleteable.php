<?php

namespace GetRepo\Generator\Generator\Entity;

use Gedmo\Mapping\Annotation\SoftDeleteable;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use GetRepo\Generator\Generator\AbstractComposition;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;

class BehaviorSoftDeleteable extends AbstractComposition
{
    public static function getPriority(): int
    {
        return 10;
    }

    public static function supports(array $mapping, array $compositionConfs): bool
    {
        return isset($mapping['behaviors']['softdeleteable']);
    }

    public function __invoke(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        array $compositionConfs,
    ): void {
        if (!isset($compositionConfs['generated'])) {
            $namespaceGenerator->addUse(SoftDeleteableEntity::class);
            $classGenerator->addTrait(SoftDeleteableEntity::class);
        } elseif ($compositionConfs['inheritance_is_last'] ?? false) {
            $namespaceGenerator->addUse(SoftDeleteable::class);
            $classGenerator->addAttribute(SoftDeleteable::class);
        }
    }
}
