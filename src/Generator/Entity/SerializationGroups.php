<?php

namespace GetRepo\Generator\Generator\Entity;

use GetRepo\Generator\Exception\MappingException;
use GetRepo\Generator\Generator\AbstractComposition;
use GetRepo\Generator\Util\MappingUtil;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;
use Symfony\Component\Serializer\Attribute\Groups;

class SerializationGroups extends AbstractComposition
{
    public static function getPriority(): int
    {
        // after validator
        return Validator::getPriority() + 1;
    }

    public static function supports(array $mapping, array $compositionConfs): bool
    {
        return true;
    }

    public function __invoke(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        array $compositionConfs,
    ): void {
        if (!isset($compositionConfs['generated'])) {
            foreach (MappingUtil::getAllFieldsAndAssocs($mapping) as $field => $fieldMapping) {
                if (count($fieldMapping['serialization']['groups'] ?? [])) {
                    $property = null;
                    // TODO duplicate code #properties_or_constructor_promotion_field
                    if ($classGenerator->hasProperty($field) || isset($mapping['isDiscriminatedBy'])) {
                        $property = $classGenerator->getProperty($field); // throw exception
                    } elseif (Constructor::hasConstructor($classGenerator)) {
                        $property = Constructor::getConstructor(classGenerator: $classGenerator)
                            ->getParameter($field); // throw exception
                    }
                    if (!$property) {
                        throw new MappingException(sprintf(
                            'Field "%s" was not found as property or constructor promotion',
                            $field,
                        ));
                    }
                    $namespaceGenerator->addUse(Groups::class);
                    $property->addAttribute(
                        Groups::class,
                        ['groups' => $fieldMapping['serialization']['groups']],
                    );
                }
            }
        }
    }
}
