<?php

namespace GetRepo\Generator\Generator\Entity;

use Gedmo\Blameable\Traits\BlameableEntity;
use GetRepo\Generator\Generator\AbstractComposition;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;

class BehaviorBlameable extends AbstractComposition
{
    public static function getPriority(): int
    {
        return 10;
    }

    public static function supports(array $mapping, array $compositionConfs): bool
    {
        // only for main class
        return !isset($compositionConfs['generated']) && isset($mapping['behaviors']['blameable']);
    }

    public function __invoke(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        array $compositionConfs,
    ): void {
        $namespaceGenerator->addUse(BlameableEntity::class);
        $classGenerator->addTrait(BlameableEntity::class);
    }
}
