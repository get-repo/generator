<?php

namespace GetRepo\Generator\Generator\Entity;

use GetRepo\Generator\Exception\MappingException;
use GetRepo\Generator\Generator\AbstractComposition;
use GetRepo\Generator\Util\MappingUtil;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\Helpers;
use Nette\PhpGenerator\Literal;
use Nette\PhpGenerator\PhpNamespace;
use Symfony\Component\Validator\Constraints\Composite;

class Validator extends AbstractComposition
{
    public static function getPriority(): int
    {
        // after behaviours
        return BehaviorBlameable::getPriority() - 1;
    }

    public static function supports(array $mapping, array $compositionConfs): bool
    {
        return true;
    }

    public function __invoke(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        array $compositionConfs,
    ): void {
        if (!isset($compositionConfs['generated'])) {
            foreach ($mapping['validators'] ?? [] as $validatorInstance) {
                $assertClass = get_class($validatorInstance);
                $assertOptions = MappingUtil::getPropertyMatches((array) $validatorInstance, $assertClass);
                $namespaceGenerator->addUse($assertClass);
                $classGenerator->addAttribute(
                    $assertClass,
                    MappingUtil::getPropertyMatches($assertOptions, $assertClass),
                );
            }
            foreach (MappingUtil::getAllFieldsAndAssocs($mapping) as $field => $fieldMapping) {
                $property = null;
                // TODO duplicate code #properties_or_constructor_promotion_field
                if ($classGenerator->hasProperty($field) || isset($mapping['isDiscriminatedBy'])) {
                    $property = $classGenerator->getProperty($field); // throw exception
                } elseif (Constructor::hasConstructor($classGenerator)) {
                    $property = Constructor::getConstructor(classGenerator: $classGenerator)
                        ->getParameter($field); // throw exception
                }
                if (!$property) {
                    throw new MappingException(sprintf(
                        'Field "%s" was not found as property or constructor promotion',
                        $field,
                    ));
                }
                foreach ($fieldMapping['validators'] ?? [] as $validatorInstance) {
                    $assertClass = get_class($validatorInstance);
                    /** @var array $validatorInstanceArray */
                    $validatorInstanceArray = (array) $validatorInstance;
                    // specific groups default value
                    if (['Default'] === ($validatorInstanceArray['groups'] ?? false)) {
                        unset($validatorInstanceArray['groups']);
                    }
                    $assertOptions = MappingUtil::getPropertyMatches($validatorInstanceArray, $assertClass);
                    $namespaceGenerator->addUse($assertClass);

                    if ($validatorInstance instanceof Composite) {
                        $allConstraints = [];
                        foreach ($validatorInstance->getNestedConstraints() as $nestedConstraint) {
                            $nestedConstraintClass = get_class($nestedConstraint);
                            $namespaceGenerator->addUse(get_class($nestedConstraint));
                            /** @var array $nestedConstraintArray */
                            $nestedConstraintArray = (array) $nestedConstraint;
                            // specific groups default value
                            if (['Default'] === ($nestedConstraintArray['groups'] ?? false)) {
                                unset($nestedConstraintArray['groups']);
                            }
                            $allConstraints[] = Literal::new(
                                Helpers::extractShortName($nestedConstraintClass),
                                MappingUtil::getPropertyMatches($nestedConstraintArray, $nestedConstraintClass),
                            );
                        }
                        $assertOptions['constraints'] = $allConstraints;
                    }
                    $property->addAttribute(
                        $assertClass,
                        MappingUtil::getPropertyMatches($assertOptions, $assertClass),
                    );
                }
            }
        }
    }
}
