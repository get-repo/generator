<?php

namespace GetRepo\Generator\Generator\Entity;

use GetRepo\Generator\Generator\AbstractComposition;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;

class Implement extends AbstractComposition
{
    public static function getPriority(): int
    {
        return Abstraction::getPriority() + 1;
    }

    public static function supports(array $mapping, array $compositionConfs): bool
    {
        return !isset($compositionConfs['generated'])
            && ($compositionConfs['implements'] || ($mapping['class']['implements'] ?? false))
            && !isset($mapping['isDiscriminatedBy']);
    }

    public function __invoke(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        array $compositionConfs,
    ): void {
        $implements = array_unique(array_merge($compositionConfs['implements'], $mapping['class']['implements'] ?? []));
        foreach ($implements as $implementsClass) {
            $namespaceGenerator->addUse($implementsClass);
            $classGenerator->addImplement($implementsClass);
        }
    }
}
