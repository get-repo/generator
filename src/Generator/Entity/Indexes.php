<?php

namespace GetRepo\Generator\Generator\Entity;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Index;
use GetRepo\Generator\Generator\AbstractComposition;
use GetRepo\Generator\Util\MappingUtil;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;

class Indexes extends AbstractComposition
{
    public static function getPriority(): int
    {
        return 10;
    }

    public static function supports(array $mapping, array $compositionConfs): bool
    {
        return $mapping['indexes'] // if we have some indexes
            // TODO duplicate check no inheritance or is last #check_no_inheritance_or_is_last
            && !isset($mapping['discriminator']) // not discriminator
            && (
                !count($compositionConfs['inheritance']) // no inheritance
                ||
                true === ($compositionConfs['inheritance_is_last'] ?? false) // last entity
            );
    }

    public function __invoke(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        array $compositionConfs,
    ): void {
        if ($mapping['indexes']) {
            $namespaceGenerator->addUse(Index::class);
            foreach ($mapping['indexes'] as $indexName => $index) {
                $classGenerator->addAttribute(
                    Index::class,
                    MappingUtil::getPropertyMatches(
                        array_merge(['name' => $indexName], array_filter($index)),
                        Index::class,
                    ),
                );
            }
        }
    }
}
