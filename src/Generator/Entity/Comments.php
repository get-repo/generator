<?php

namespace GetRepo\Generator\Generator\Entity;

use GetRepo\Generator\Generator\AbstractComposition;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;

class Comments extends AbstractComposition
{
    public static function getPriority(): int
    {
        return 30;
    }

    public static function supports(array $mapping, array $compositionConfs): bool
    {
        // only for main class
        return !isset($compositionConfs['generated']) && isset($compositionConfs['comments']);
    }

    public function __invoke(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        array $compositionConfs,
    ): void {
        foreach ($compositionConfs['comments']['class'] ?? [] as $comment) {
            $classGenerator->addComment($comment);
        }
    }
}
