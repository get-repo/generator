<?php

namespace GetRepo\Generator\Generator\Entity;

use Doctrine\Common\Collections\Collection;
use GetRepo\Generator\Exception\MappingException;
use GetRepo\Generator\Generator\AbstractComposition;
use GetRepo\Generator\Util\MappingUtil;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;

class GettersSetters extends AbstractComposition
{
    public static function getPriority(): int
    {
        return Methods::getPriority() + 1;
    }

    public static function supports(array $mapping, array $compositionConfs): bool
    {
        return ($compositionConfs['methods']['auto_getters'] || $compositionConfs['methods']['auto_setters'])
            && MappingUtil::hasFieldOrAssoc($mapping);
    }

    public function __invoke(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        array $compositionConfs,
    ): void {
        if (!isset($compositionConfs['generated'])) {
            $fields = array_keys(MappingUtil::getAllFields($mapping));
            $assocTypeMap = [];
            foreach (MappingUtil::getAllAssocs($mapping, true) as $type => $names) {
                foreach (array_keys($names) as $name) {
                    $assocTypeMap[$name] = $type;
                }
            }

            foreach (MappingUtil::getAllFieldsAndAssocs($mapping) as $name => $fieldMapping) {
                if (in_array($name, $fields)) {
                    $type = MappingUtil::getTypeHint($fieldMapping['type'] ?? null);
                    if ($fieldMapping['enum']) {
                        $namespaceGenerator->addUse($fieldMapping['enum']);
                        $type = $fieldMapping['enum'];
                    }
                    $getter = $classGenerator->addMethod(sprintf('get%s', ucfirst($name)));
                    $getter->setReturnType($type);
                    if ($fieldMapping['nullable'] ?? false) {
                        $getter->setReturnNullable();
                    }
                    $getter->setBody(sprintf('return $this->%s;', $name));

                    $setter = $classGenerator->addMethod(sprintf('set%s', ucfirst($name)));
                    $parameter = $setter->addParameter($name);
                    $parameter->setType($type);
                    if ($fieldMapping['nullable'] ?? false) {
                        $parameter->setNullable();
                    }
                    $setter->setReturnType('self');
                    $setter->addBody(sprintf('$this->%1$s = $%1$s;', $name));
                    $setter->addBody('');
                    $setter->addBody('return $this;');
                    if ('array' === $type) {
                        $getter->addComment('@return array<mixed>');
                        $setter->addComment(sprintf('@param array<mixed> $%s', $name));
                    }
                } elseif (isset($assocTypeMap[$name])) {
                    switch ($assocTypeMap[$name]) {
                        case MappingUtil::ONE_TO_ONE:
                        case MappingUtil::MANY_TO_ONE:
                            $getter = $classGenerator->addMethod(sprintf('get%s', ucfirst($name)));
                            $getter->setReturnType($fieldMapping['targetEntity']);
                            $getter->setReturnNullable($fieldMapping['joinColumn']['nullable'] ?? true);
                            $getter->setBody(sprintf('return $this->%s;', $name));

                            $setter = $classGenerator->addMethod(sprintf('set%s', ucfirst($name)));
                            $setter->addParameter($name)->setType($fieldMapping['targetEntity']);
                            $setter->setReturnType('self');
                            $setter->addBody(sprintf('$this->%1$s = $%1$s;', $name));
                            $setter->addBody('');
                            $setter->addBody('return $this;');
                            break;


                        case MappingUtil::MANY_TO_MANY:
                            // TODO with join table
                            throw new \Exception('Finish to implement MANY TO MANY getters and setters');
                        case MappingUtil::ONE_TO_MANY:
                            $singular = $this->inflector->singularize($name)[0];
                            $getter = $classGenerator->addMethod(sprintf('get%s', ucfirst($name)));
                            $getter->setReturnType(Collection::class);
                            $getter->addBody(sprintf('if (!isset($this->%s)) {', $name));
                            $getter->addBody(sprintf('    $this->%s = new ArrayCollection();', $name));
                            $getter->addBody('}');
                            $getter->addBody('');
                            $getter->addBody(sprintf('return $this->%s;', $name));

                            $adder = $classGenerator->addMethod(sprintf('add%s', ucfirst($singular)));
                            $adder->addParameter($name)->setType($fieldMapping['targetEntity']);
                            $adder->setReturnType('self');
                            $adder->addBody("if (!\$this->{$name}->contains(\${$name})) {");
                            $adder->addBody("    \$this->{$name}->add(\${$name});");
                            $adder->addBody('}');
                            $adder->addBody('');
                            $adder->addBody('return $this;');

                            $remover = $classGenerator->addMethod(sprintf('remove%s', ucfirst($singular)));
                            $remover->addParameter($name)->setType($fieldMapping['targetEntity']);
                            $remover->setReturnType('self');
                            $remover->addBody("\$this->{$name}->removeElement(\${$name});");
                            $remover->addBody('');
                            $remover->addBody('return $this;');
                            break;
                    }
                } else {
                    throw new MappingException("Field \"{$name}\" was not handled correctly");
                }
            }
        }
    }
}
