<?php

namespace GetRepo\Generator\Generator\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use GetRepo\Generator\Generator\AbstractComposition;
use GetRepo\Generator\Util\MappingUtil;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\Method;
use Nette\PhpGenerator\PhpNamespace;

class Constructor extends AbstractComposition
{
    public const METHOD_NAME = '__construct';

    public static function getPriority(): int
    {
        return 80;
    }

    public static function supports(array $mapping, array $compositionConfs): bool
    {
        return !isset($mapping['isDiscriminatedBy'])
            && (
                isset($compositionConfs['constructor'])
                || ConstructorPromotion::supports($mapping, $compositionConfs)
                || count($mapping[MappingUtil::ONE_TO_MANY]) > 0
                || count($mapping[MappingUtil::MANY_TO_MANY]) > 0
            );
    }

    public static function getConstructor(ClassType $classGenerator): Method
    {
        if (self::hasConstructor($classGenerator)) {
            return $classGenerator->getMethod(self::METHOD_NAME);
        }

        return $classGenerator->addMethod(self::METHOD_NAME);
    }

    public static function hasConstructor(ClassType $classGenerator): bool
    {
        return $classGenerator->hasMethod(self::METHOD_NAME);
    }

    public function __invoke(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        array $compositionConfs,
    ): void {
        if (!isset($compositionConfs['generated'])) {
            $this->addCollection(
                classGenerator: $classGenerator,
                namespaceGenerator: $namespaceGenerator,
                assocMappings: $mapping[MappingUtil::ONE_TO_MANY] ?? [],
            );
            $this->addCollection(
                classGenerator: $classGenerator,
                namespaceGenerator: $namespaceGenerator,
                assocMappings: $mapping[MappingUtil::MANY_TO_MANY] ?? [],
            );
            if (isset($compositionConfs['constructor']) && $compositionConfs['constructor']) {
                $constructor = self::getConstructor(classGenerator: $classGenerator);
                if ($compositionConfs['constructor']['body'] ?? false) {
                    $constructor->addBody($compositionConfs['constructor']['body']);
                }
            }
        }
    }

    private function addCollection(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $assocMappings,
    ): void {
        $constructor = self::getConstructor(classGenerator: $classGenerator);
        foreach (array_keys($assocMappings) as $name) {
            $namespaceGenerator->addUse(ArrayCollection::class);
            $constructor->addBody("\$this->{$name} = new ArrayCollection();");
        }
    }
}
