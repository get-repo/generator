<?php

namespace GetRepo\Generator\Generator\Entity;

use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\Table;
use Gedmo\Sortable\Entity\Repository\SortableRepository;
use GetRepo\DoctrineExtension\OneRow\Service\OneRowRepository;
use GetRepo\Generator\Generator\AbstractComposition;
use GetRepo\Generator\Util\MappingUtil;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\Literal;
use Nette\PhpGenerator\PhpNamespace;

class Discriminator extends AbstractComposition
{
    public static function getPriority(): int
    {
        return 40;
    }

    public static function supports(array $mapping, array $compositionConfs): bool
    {
        return isset($mapping['discriminator']);
    }

    public function __invoke(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        array $compositionConfs,
    ): void {
        $classGenerator->setAbstract();
        if (!isset($compositionConfs['generated'])) {
            $discriminator = $mapping['discriminator'];
            $entityAttributeOptions = $tableAttributeOptions = [];
            // table name
            if ($table = ($mapping['table'] ?? null)) {
                $tableAttributeOptions['name'] = $table;
            }
            // table options
            if ($tableOptions = $mapping['options']) {
                $tableAttributeOptions['options'] = $tableOptions;
            }
            // sortable repository
            if ($mapping['behaviors']['sortable'] ?? false) {
                $namespaceGenerator->addUse(SortableRepository::class);
                $entityAttributeOptions['repositoryClass'] = new Literal('SortableRepository::class');
            }
            // onerow repository
            if ($mapping['behaviors']['onerow'] ?? false) {
                $namespaceGenerator->addUse(OneRowRepository::class);
                $entityAttributeOptions['repositoryClass'] = new Literal('OneRowRepository::class');
            }
            $namespaceGenerator->addUse(Entity::class);
            $classGenerator->addAttribute(
                Entity::class,
                MappingUtil::getPropertyMatches($entityAttributeOptions, Entity::class),
            );
            $namespaceGenerator->addUse(Table::class);
            $classGenerator->addAttribute(
                Table::class,
                MappingUtil::getPropertyMatches($tableAttributeOptions, Table::class),
            );
            $namespaceGenerator->addUse(InheritanceType::class);
            $classGenerator->addAttribute(InheritanceType::class, [$discriminator['type']]);
            $namespaceGenerator->addUse(DiscriminatorColumn::class);
            $classGenerator->addAttribute(
                DiscriminatorColumn::class,
                MappingUtil::getPropertyMatches($discriminator['column'] ?? [], DiscriminatorColumn::class),
            );
            $map = $discriminator['map'];
            foreach ($map as &$entity) {
                $namespaceGenerator->addUse($entity);
                $parts = explode('\\', $entity);
                $entity = new Literal(end($parts) . '::class');
            }
            $namespaceGenerator->addUse(DiscriminatorMap::class);
            $classGenerator->addAttribute(DiscriminatorMap::class, [$map]);
        }
    }
}
