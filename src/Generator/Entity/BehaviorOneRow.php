<?php

namespace GetRepo\Generator\Generator\Entity;

use Gedmo\Mapping\OneRow;
use GetRepo\Generator\Generator\AbstractComposition;
use GetRepo\Generator\Util\MappingUtil;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;

class BehaviorOneRow extends AbstractComposition
{
    public static function getPriority(): int
    {
        return 10;
    }

    public static function supports(array $mapping, array $compositionConfs): bool
    {
        // only for main class
        return !isset($compositionConfs['generated']) && isset($mapping['behaviors']['onerow']);
    }

    public function __invoke(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        array $compositionConfs,
    ): void {
        $namespaceGenerator->addUse(OneRow::class);
        $classGenerator->addAttribute(
            OneRow::class,
            MappingUtil::getPropertyMatches($mapping['behaviors']['onerow'], OneRow::class),
        );
    }
}
