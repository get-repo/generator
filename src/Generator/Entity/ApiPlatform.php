<?php

namespace GetRepo\Generator\Generator\Entity;

use ApiPlatform\Metadata\ApiResource;
use GetRepo\Generator\Generator\AbstractComposition;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\Helpers;
use Nette\PhpGenerator\Literal;
use Nette\PhpGenerator\PhpNamespace;

class ApiPlatform extends AbstractComposition
{
    public static function getPriority(): int
    {
        return min(Constructor::getPriority(), Doctrine::getPriority()) - 1;
    }

    public static function supports(array $mapping, array $compositionConfs): bool
    {
        return !isset($compositionConfs['generated']) && isset($mapping['api']) && $mapping['api'];
    }

    public function __invoke(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        array $compositionConfs,
    ): void {
        $api = $mapping['api'];
        if ($api['operations'] ?? false) {
            $operations = [];
            foreach ($api['operations'] as $operationClass => $operation) {
                $namespaceGenerator->addUse($operationClass);
                $operation['class'] = $mapping['name'];
                $parts = explode('\\', $operationClass);
                $operations[] = Literal::new(end($parts), $operation);
            }
            $api['operations'] = $operations;
        }
        if (!isset($api['shortName'])) {
            $api['shortName'] = Helpers::extractShortName($mapping['name']);
        }
        $namespaceGenerator->addUse(ApiResource::class);
        $classGenerator->addAttribute(ApiResource::class, $api);
    }
}
