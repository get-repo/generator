<?php

namespace GetRepo\Generator\Generator\Entity;

use Gedmo\Mapping\Ownable;
use GetRepo\Generator\Generator\AbstractComposition;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;

class BehaviorOwnable extends AbstractComposition
{
    public static function getPriority(): int
    {
        return 10;
    }

    public static function supports(array $mapping, array $compositionConfs): bool
    {
        return !isset($compositionConfs['generated']) // only for main class
            && isset($mapping['behaviors']['ownable']); // has ownable behavior
    }

    public function __invoke(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        array $compositionConfs,
    ): void {
        $ownable = $mapping['behaviors']['ownable'];
        $field = $ownable['field_name'];
        // TODO duplicate code #properties_or_constructor_promotion_field
        if ('constructor_property_promotion' !== ($compositionConfs['properties']['define_as'] ?? null)) {
            $property = $classGenerator->getProperty($field); // throw exception
        } else {
            // TODO This is not very nice to convert constructor promotion to property
            // convert constructor promotion to property
            $constructor = Constructor::getConstructor(classGenerator: $classGenerator);
            $parameter = $constructor->getParameter($field); // throw exception
            $property = $classGenerator->addProperty($field);
            $property->setType($parameter->getType());
            $property->setNullable($parameter->isNullable());
            $property->setAttributes($parameter->getAttributes());
            $property->setValue($parameter->getDefaultValue());
            $property->setComment($parameter->getComment());
            $property->setVisibility('protected');
            $constructor->removeParameter($field);
            // if constructor is empty, we delete it
            if (!$constructor->getParameters() && !$constructor->getBody()) {
                $classGenerator->removeMethod($constructor->getName());
            }
        }
        $namespaceGenerator->addUse(Ownable::class);
        $property->addAttribute(Ownable::class);
    }
}
