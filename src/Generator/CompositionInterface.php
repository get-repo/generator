<?php

namespace GetRepo\Generator\Generator;

use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;

interface CompositionInterface
{
    public static function getPriority(): int;

    public static function supports(
        array $mapping,
        array $compositionConfs,
    ): bool;

    public function __invoke(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        array $compositionConfs,
    ): void;
}
