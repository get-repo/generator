<?php

namespace GetRepo\Generator\Generator;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OrderBy;
use GetRepo\Generator\Configuration\EntityConfiguration;
use GetRepo\Generator\Exception\MappingException;
use GetRepo\Generator\Generator\Entity\Constructor;
use GetRepo\Generator\Util\MappingUtil;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\Helpers;
use Nette\PhpGenerator\Literal;
use Nette\PhpGenerator\PhpNamespace;
use Nette\PhpGenerator\PromotedParameter;
use Nette\PhpGenerator\Property;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\String\Inflector\EnglishInflector;
use Symfony\Component\String\Inflector\InflectorInterface;

abstract class AbstractComposition implements CompositionInterface
{
    protected InflectorInterface $inflector;

    public function __construct(
        #[Autowire(param: 'getrepo_generator.config')]
        protected array $config,
    ) {
        $this->inflector = new EnglishInflector();
    }

    protected function generateFieldsAndAssoc(
        ClassType $classGenerator,
        PhpNamespace $namespaceGenerator,
        array $mapping,
        string $visibility,
        callable $parameterBuilder,
        array $options = [],
    ): void {
        $optionsResolver = new OptionsResolver();
        $optionsResolver->setDefault('doctrine_attributes', true);
        $options = $optionsResolver->resolve($options);

        $fields = array_keys(MappingUtil::getAllFields($mapping));
        $assocTypeMap = [];
        foreach (MappingUtil::getAllAssocs($mapping, true) as $type => $names) {
            foreach (array_keys($names) as $name) {
                $assocTypeMap[$name] = $type;
            }
        }

        foreach (MappingUtil::getAllFieldsAndAssocs($mapping) as $name => $fieldMapping) {
            if (in_array($name, [EntityConfiguration::SLUGGABLE_FIELD, EntityConfiguration::SORTABLE_FIELD])) {
                continue;
            }
            if (in_array($name, $fields)) {
                /** @var PromotedParameter|Property $parameter */
                $parameter = $parameterBuilder($name);
                // doctrine column options
                if ($options['doctrine_attributes']) {
                    if ($fieldMapping['enum']) {
                        $namespaceGenerator->addUse($fieldMapping['enum']);
                        $fieldMapping['enumType'] = new Literal(
                            Helpers::extractShortName($fieldMapping['enum']) . '::class'
                        );
                    }
                    $namespaceGenerator->addUse(Column::class);
                    $parameter->addAttribute(
                        Column::class,
                        MappingUtil::getPropertyMatches((array) $fieldMapping, Column::class),
                    );
                    unset($fieldMapping['enumType']);
                }
                // property visibility
                if (method_exists($parameter, 'setVisibility')) {
                    $parameter->setVisibility($visibility);
                }
                // type hint
                if ($fieldMapping['enum']) {
                    $namespaceGenerator->addUse($fieldMapping['enum']);
                    $parameter->setType($fieldMapping['enum']);
                } else {
                    $parameter->setType(MappingUtil::getTypeHint($fieldMapping['type'] ?? null));
                }
                // nullable
                if ($fieldMapping['nullable'] ?? false) {
                    $parameter->setNullable();
                    if (!$parameter instanceof Property) {
                        $parameter->setDefaultValue(null);
                    } else {
                        $parameter->setValue(null);
                    }
                }
                // default value
                if (isset($fieldMapping['options']['default'])) {
                    if (!$parameter instanceof Property) {
                        $parameter->setDefaultValue($fieldMapping['options']['default']);
                    } else {
                        $parameter->setValue($fieldMapping['options']['default']);
                    }
                }
            } elseif (isset($assocTypeMap[$name])) {
                $assocType = $assocTypeMap[$name];
                // oneToMany relationship is always a property
                if (MappingUtil::ONE_TO_MANY === $assocType) {
                    if ($classGenerator->hasProperty($name)) {
                        $parameter = $classGenerator->getProperty($name);
                        $forceDoctrineAttributes = true;
                    } else {
                        $parameter = $classGenerator->addProperty($name);
                    }
                } else {
                    $parameter = $parameterBuilder($name);
                }
                // join column options
                $joinColumnOptions = MappingUtil::getPropertyMatches(
                    $fieldMapping['joinColumn'] ?? [],
                    JoinColumn::class,
                );
                unset($fieldMapping['joinColumn']);
                if ($options['doctrine_attributes'] && $joinColumnOptions) {
                    $namespaceGenerator->addUse(JoinColumn::class);
                    $parameter->addAttribute(JoinColumn::class, $joinColumnOptions);
                }
                // assoc column options
                $assocClass = MappingUtil::getAssocClass($assocType);
                $assocOptions = MappingUtil::getPropertyMatches($fieldMapping, $assocClass);
                if ($assocOptions['targetEntity']) {
                    $namespaceGenerator->addUse($assocOptions['targetEntity']);
                    $assocOptions['targetEntity'] = new Literal(
                        Helpers::extractShortName($assocOptions['targetEntity']) . '::class',
                    );
                }
                if ($options['doctrine_attributes']) {
                    $namespaceGenerator->addUse($assocClass);
                    $parameter->addAttribute($assocClass, $assocOptions);
                }
                // property visibility
                if (method_exists($parameter, 'setVisibility')) {
                    $parameter->setVisibility($visibility);
                }

                switch ($assocType) {
                    case MappingUtil::ONE_TO_ONE:
                    case MappingUtil::MANY_TO_ONE:
                        $parameter->setType($fieldMapping['targetEntity']);
                        $parameter->setNullable($joinColumnOptions['nullable'] ?? true);
                        if ($parameter->isNullable()) {
                            if (!$parameter instanceof Property) {
                                $parameter->setDefaultValue(null);
                            } else {
                                $parameter->setValue(null);
                            }
                        }
                        break;

                    case MappingUtil::MANY_TO_MANY:
                        // TODO with join table
                        throw new \Exception('Finish to implement MANY TO MANY in ' . __FUNCTION__);
                    case MappingUtil::ONE_TO_MANY:
                        // order by remove because own attribute
                        $orderBy = $fieldMapping['orderBy'] ?? null;
                        unset($fieldMapping['orderBy']);

                        $namespaceGenerator->addUse(Collection::class);
                        $namespaceGenerator->addUse(ArrayCollection::class);
                        $namespaceGenerator->addUse($fieldMapping['targetEntity']);
                        $comment = sprintf(
                            '@var Collection<%s>',
                            Helpers::extractShortName($fieldMapping['targetEntity']),
                        );
                        if (!str_contains((string)$parameter->getComment(), $comment)) {
                            $parameter->addComment($comment);
                        }
                        $parameter->setType(Collection::class);
                        // order by
                        if ($orderBy && $parameter->getAttributes()) {
                            $namespaceGenerator->addUse(OrderBy::class);
                            $parameter->addAttribute(OrderBy::class, [$orderBy]);
                        }

                        break;
                }
            } else {
                throw new MappingException("Field \"{$name}\" was not handled correctly");
            }
            // phpstan array type definition
            if ('array' === $parameter->getType()) {
                if (!$parameter instanceof Property) {
                    $constructor = Constructor::getConstructor(classGenerator: $classGenerator);
                    $constructor->addComment(sprintf('@param array<mixed> $%s', $parameter->getName()));
                } else {
                    $parameter->addComment('@var array<mixed>');
                }
            }
        }
    }
}
