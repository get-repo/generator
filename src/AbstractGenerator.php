<?php

namespace GetRepo\Generator;

use GetRepo\Generator\Generator\CompositionInterface;
use GetRepo\Generator\Generator\Generated;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;
use Nette\PhpGenerator\Printer;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\DependencyInjection\Attribute\TaggedIterator;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\String\ByteString;

abstract class AbstractGenerator
{
    private array $config;
    /** @var CompositionInterface[] */
    private array $compositions = [];

    abstract protected function buildConfiguration(): ConfigurationInterface;

    public function __construct(
        #[Autowire(param: 'getrepo_generator.config')]
        array $config,
        #[TaggedIterator('getrepo_generator.composition', defaultPriorityMethod: 'getPriority')]
        iterable $compositions
    ) {
        $className = new ByteString(static::class);
        $type = strtolower($className->match('/\\\(\w+)Generator$/')[1] ?? false);
        if (!isset($config[$type])) {
            throw new \RuntimeException(
                "Generator config was not found.\n" .
                "Maybe you should define a config file \"config/packages/generator.yaml\" ?."
            );
        }
        $this->config = $config[$type];
        foreach ($compositions as $composition) {
            $matches = (new ByteString(get_class($composition)))->match('/\\\([^\\\]+)\\\(\w+)$/');
            if ($type === strtolower($matches[1] ?? false)) {
                $this->compositions[(string) (new ByteString($matches[2]))->snake()] = $composition;
            }
        }
    }

    public function generate(string $shortName, array $mapping, array $options = []): array
    {
        $resolver = new OptionsResolver();
        $resolver->setDefault('profile', $this->config['default']);
        $resolver->setAllowedValues('profile', array_merge([null], array_keys($this->config['profiles'])));
        $resolver->setDefault('replacements', []);
        $resolver->setAllowedTypes('replacements', 'array');
        $options = $resolver->resolve($options);

        $processor = new Processor();
        $mapping = $processor->processConfiguration($this->buildConfiguration(), [$mapping]);
        $profileConfig = $this->config['profiles'][$options['profile']] ?? false;
        if (!is_array($profileConfig)) {
            throw new \RuntimeException(sprintf(
                'Generator profile config "%s" was not found. Available profiles are : "%s"',
                $options['profile'],
                implode('", "', array_keys($this->config['profiles'])),
            ));
        }

        if ($options['replacements']) {
            $this->replace($profileConfig, $options['replacements']);
        }

        $namespace = $profileConfig['namespace'];
        $namespaceGenerator = new PhpNamespace($namespace);
        $classGenerator = new ClassType($shortName);
        $namespaceGenerator->add($classGenerator);

        $generatedItems = [];
        foreach ($this->compositions as $composition) {
            if ($composition->supports(mapping: $mapping, compositionConfs: $profileConfig)) {
                $composition( // call invoke method
                    classGenerator: $classGenerator,
                    namespaceGenerator: $namespaceGenerator,
                    mapping: $mapping,
                    compositionConfs: $profileConfig,
                );
            }
        }

        $generated = new Generated(
            classGenerator: $classGenerator,
            namespaceGenerator: $namespaceGenerator,
        );
        $generatedItems[$generated->getFullClassname()] = $generated;

        if ($profileConfig['inheritance'] ?? false) {
            $profileConfig['generated'] = $generatedItems;
            $profileConfig['inheritance_count'] = count($profileConfig['inheritance']);
            foreach (array_keys($profileConfig['inheritance']) as $i => $namespace) {
                $i++;
                $profileConfig['inheritance_iterator'] = $i;
                $isLast = $profileConfig['inheritance_count'] === $profileConfig['inheritance_iterator'];
                $profileConfig['inheritance_is_last'] = $isLast;
                $namespaceGenerator = new PhpNamespace($namespace);
                $classGenerator = new ClassType($shortName);
                $namespaceGenerator->add($classGenerator);
                foreach ($this->compositions as $composition) {
                    if ($composition->supports(mapping: $mapping, compositionConfs: $profileConfig)) {
                        $composition( // call invoke method
                            classGenerator: $classGenerator,
                            namespaceGenerator: $namespaceGenerator,
                            mapping: $mapping,
                            compositionConfs: $profileConfig,
                        );
                    }
                }

                $generated = new Generated(
                    classGenerator: $classGenerator,
                    namespaceGenerator: $namespaceGenerator,
                );
                $generatedItems[$generated->getFullClassname()] = $generated;
                $profileConfig['generated'] = $generatedItems;
            }
        }

        return array_map(
            function (Generated $generated) {
                $printer = new Printer();
                $printer->indentation = '    ';
                $printer->linesBetweenMethods = 1;
                $printer->wrapLength = 100;

                return $generated->print($printer);
            },
            $generatedItems,
        );
    }

    private function replace(array &$data, array $replacements): void
    {
        $replacer = function (mixed &$v) use ($replacements): void {
            if (is_scalar($v)) {
                $v = str_replace(array_keys($replacements), array_values($replacements), $v);
            }
        };
        foreach ($data as $k => $v) {
            if (is_iterable($v)) {
                $this->replace($v, $replacements);
            }
            unset($data[$k]);
            $replacer($k);
            $replacer($v);
            $data[$k] = $v;
        }
    }
}
