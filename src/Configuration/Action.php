<?php

namespace GetRepo\Generator\Configuration;

enum Action: string
{
    case LIST = 'list';
    case NEW = 'new';
    case EDIT = 'edit';
    case SHOW = 'show';
    case DUPLICATE = 'duplicate';
    case DELETE = 'delete';
    case TRASH = 'trash';
    case RECOVER = 'recover';

    public static function getColors(): array
    {
        $colors = array_map(function ($action): string {
            return $action->getColor();
        }, self::cases());
        sort($colors);

        return array_values(array_unique($colors));
    }

    public static function getCruds(bool $softDelete = false): array
    {
        return array_values(array_filter(array_map(function ($action) use ($softDelete): ?string {
            return $action->isCrud($softDelete) ? $action->value : null;
        }, self::cases())));
    }

    public static function getForms(): array
    {
        return array_values(array_filter(array_map(function ($action): ?string {
            return $action->hasForm() ? $action->value : null;
        }, self::cases())));
    }

    public static function getWithIds(bool $softDelete = false): array
    {
        return array_values(array_filter(array_map(function ($action) use ($softDelete): ?string {
            return $action->isWithIds($softDelete) ? $action->value : null;
        }, self::cases())));
    }

    public function isList(): bool
    {
        return in_array($this, [self::LIST, self::TRASH], true);
    }

    public function isSoftDeleteAction(): bool
    {
        return in_array($this, [self::TRASH, self::RECOVER], true);
    }

    public function hasForm(): bool
    {
        return in_array($this, [self::NEW, self::EDIT, self::SHOW], true);
    }

    public function isCrud(bool $softDelete = false): bool
    {
        return $softDelete
            ? // CRUD with soft delete
            !$this->isList()
            : // CRUD without soft delete
            !$this->isList() && !$this->isSoftDeleteAction();
    }

    public function isWithIds(bool $softDelete = false): bool
    {
        return $this->isCrud($softDelete) && self::NEW !== $this;
    }

    public function getColor(): string
    {
        return match ($this) {
            self::LIST => 'light',
            self::NEW => 'success',
            self::EDIT => 'primary',
            self::SHOW => 'info',
            self::DUPLICATE => 'dark',
            self::DELETE => 'danger',
            self::TRASH => 'danger',
            self::RECOVER => 'warning',
            // @phpstan-ignore-next-line match.unreachable
            default => throw new \LogicException(sprintf('Color is missing for action "%s"', $this->name))
        };
    }

    public function getSubmitClass(): ?string
    {
        if ($color = $this->getColor()) {
            return sprintf('btn btn-sm btn-outline-%1$s bg-%1$s text-white', $color);
        }

        return null;
    }
}
