<?php

namespace GetRepo\Generator\Configuration;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Ownable;
use GetRepo\DoctrineExtension\Configuration\SettingsableConfiguration;
use GetRepo\FormYaml\Configuration\FormConfiguration;
use GetRepo\FormYaml\Resolver\Resolver;
use Symfony\Component\Config\Definition\Builder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Exception\Exception as ConfigException;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Workflow\Registry as WorkflowRegistry;

class EntityConfiguration implements ConfigurationInterface
{
    // doctrine relationships
    public const ONE_TO_ONE = 'oneToOne';
    public const ONE_TO_MANY = 'oneToMany';
    public const MANY_TO_ONE = 'manyToOne';
    public const MANY_TO_MANY = 'manyToMany';
    // behaviour fields
    public const WORKFLOW_FIELD = 'status';
    public const SORTABLE_FIELD = 'position';
    public const SLUGGABLE_FIELD = 'slug';

    /** @var \Symfony\Component\Workflow\WorkflowInterface[] */
    protected array $workflows = [];

    public function __construct(
        #[Autowire(param: 'getrepo_generator.config')]
        private array $config,
        private Resolver $resolver,
        WorkflowRegistry $workflowRegistry,
    ) {
        $rProperty = new \ReflectionProperty($workflowRegistry, 'workflows');
        /**@var \Symfony\Component\Workflow\StateMachine $workflow */
        foreach ($rProperty->getValue($workflowRegistry) as [$workflow]) {
            $this->workflows[$workflow->getName()] = $workflow;
        }
    }

    public function getConfigTreeBuilder(): Builder\TreeBuilder
    {
        $treeBuilder = new Builder\TreeBuilder('entity_mapping');
        /** @var Builder\ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            // validate keys isDiscriminatedBy and (discriminator or extends) are incompatible
            ->validate()
                ->ifTrue(function ($mapping) {
                    return isset($mapping['isDiscriminatedBy']) && isset($mapping['discriminator']);
                })
                ->thenInvalid('Keys "isDiscriminatedBy" and "discriminator" are incompatible')
                ->ifTrue(function ($mapping) {
                    return isset($mapping['isDiscriminatedBy']) && isset($mapping['class']['extends']);
                })
                ->thenInvalid('Keys "isDiscriminatedBy" and "extends" are incompatible')
            ->end()
            // workflow : add status field if workflow exists
            ->beforeNormalization()
                ->ifTrue(function ($mapping) {
                    return $this->workflows[$mapping['name'] ?? null] ?? false;
                })
                ->then(function ($mapping) {
                    $workflow = $this->workflows[$mapping['name']]->getDefinition();
                    if (!isset($mapping['fields'][self::WORKFLOW_FIELD])) {
                        $mapping['fields'][self::WORKFLOW_FIELD] = [
                            'type' => 'string',
                            'nullable' => false,
                            'options' => [
                                'default' => current($workflow->getInitialPlaces()),
                            ],
                            'form' => [
                                'type' => $this->config['behaviors']['workflow']['form_type'] ?? null,
                                'condition' => 'action == "edit"',
                                'options' => [
                                    'priority' => 100, // first
                                    'label' => 'Status (Workflow)',
                                ],
                            ],
                        ];

                        $mapping['indexes']['search_status_idx'] = [
                            'columns' => ['status'],
                        ];
                    }

                    return $mapping;
                })
            ->end()
            // sortable: add position field if behaviour exists
            ->beforeNormalization() // beforeNormalization is ok, not validate()
                ->ifTrue(function ($mapping) {
                    return array_key_exists('sortable', $mapping['behaviors'] ?? []);
                })
                ->then(function ($mapping) {
                    if (!isset($mapping['fields'][self::SORTABLE_FIELD])) {
                        $sortable = $mapping['behaviors']['sortable'];
                        $mapping['fields'][self::SORTABLE_FIELD] = [
                            'type' => 'integer',
                            'nullable' => false,
                            'form' => ($sortable['form'] ?? true) ? [
                                'type' => IntegerType::class,
                            ] : false,
                            'validators' => [[Type::class => ['type' => 'integer']]],
                        ];
                    }

                    return $mapping;
                })
            ->end()
            // sluggable: add slug field if behaviour exists
            ->beforeNormalization() // beforeNormalization is ok, not validate()
                ->ifTrue(function ($mapping) {
                    return $mapping['behaviors']['sluggable'] ?? false;
                })
                ->then(function ($mapping) {
                    $sluggable = $mapping['behaviors']['sluggable'];
                    if (!isset($mapping['fields'][self::SLUGGABLE_FIELD])) {
                        $mapping['fields'][self::SLUGGABLE_FIELD] = [
                            'type' => 'string',
                            'length' => 128,
                            'unique' => $sluggable['unique'] ?? true,
                            'nullable' => false,
                            'form' => $sluggable['form'] ?? false,
                            'validators' => $sluggable['validators'] ?? [[NotBlank::class => null]],
                        ];
                    }

                    $mapping['indexes']['search_slug_idx'] = [
                        'columns' => ['slug'],
                    ];

                    return $mapping;
                })
            ->end()
            // ownable: add user field
            ->validate()
                ->ifTrue(function ($mapping) {
                    return array_key_exists('ownable', $mapping['behaviors'] ?? []);
                })
                ->then(function ($mapping) {
                    $userClass = $this->config['behaviors']['ownable']['user_class'] ?? false;
                    if (!$userClass) {
                        throw new ConfigException('ownable "user_class" needs to be set to use this behavior');
                    }
                    $ownable = $mapping['behaviors']['ownable'];
                    $name = $ownable['field_name'];
                    if (!isset($mapping['fields'][$name])) {
                        $form = $ownable['form'];
                        if (!array_key_exists('condition', $form)) {
                            $form['condition'] = 'loggedin_user ' .
                                'and authorization_checker.isGranted("ROLE_ADMIN", loggedin_user)';
                        }
                        if (!array_key_exists('type', $form)) {
                            $form['type'] = 'Entity';
                            $form['options']['class'] = $userClass;
                        }
                        $mapping[self::MANY_TO_ONE][$name] = [
                            'targetEntity' => $userClass,
                            'joinColumn' => [
                                'name' => $name,
                                'referencedColumnName' => 'id',
                                'nullable' => $ownable['nullable'],
                            ],
                            'form' => $form,
                        ];
                    }

                    return $mapping;
                })
            ->end()
            ->children()
                // full entity name (set manually)
                ->scalarNode('name')
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end()
                // table name
                ->scalarNode('table')
                    ->cannotBeEmpty()
                ->end()
                // indexes
                ->arrayNode('indexes')
                    ->arrayPrototype()
                        ->validate()
                            ->ifTrue(function (array $index): bool {
                                return !$index['columns'] && !$index['fields'];
                            })
                            ->thenInvalid('Index needs at least "columns" or "fields" to be set')
                        ->end()
                        ->children()
                            ->scalarNode('name')->end()
                            ->arrayNode('columns')
                                ->scalarPrototype()->end()
                            ->end()
                            ->arrayNode('fields')
                                ->scalarPrototype()->end()
                            ->end()
                            ->arrayNode('flags')
                                ->scalarPrototype()->end()
                            ->end()
                            ->arrayNode('options')
                                ->variablePrototype()->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                // table options
                ->arrayNode('options')
                    ->scalarPrototype()->end()
                ->end()
                ->scalarNode('isDiscriminatedBy')->end()
                // inheritance type
                ->arrayNode('discriminator')
                    ->children()
                        ->enumNode('type')
                            ->cannotBeEmpty()
                            ->defaultValue('SINGLE_TABLE')
                            ->values(['NONE', 'JOINED', 'SINGLE_TABLE', 'TABLE_PER_CLASS'])
                        ->end()
                        ->arrayNode('column')
                            ->children()
                                ->scalarNode('name')->end()
                                ->scalarNode('type')->end()
                                ->integerNode('length')->end()
                                ->scalarNode('columnDefinition')->end()
                                ->scalarNode('enumType')->end()
                                ->arrayNode('options')
                                    ->variablePrototype()->end()
                                ->end()
                            ->end()
                        ->end()
                        ->arrayNode('map')
                            ->cannotBeEmpty()
                            ->scalarPrototype()->end()
                        ->end()
                    ->end()
                ->end()
                // behaviors
                ->arrayNode('behaviors')
                    ->children()
                        // softdelete behavior with deletedAt datetime field
                        ->booleanNode('softdeleteable')->end()
                        // timestampable behavior with createdAt and updatedAt fields
                        ->booleanNode('timestampable')->end()
                        // blameable behavior with createdBy and updatedBy fields
                        ->booleanNode('blameable')->end()
                        // ownable behavior
                        ->arrayNode('ownable')
                            ->children()
                                ->scalarNode('field_name')
                                    ->cannotBeEmpty()
                                    ->defaultValue('owner')
                                ->end()
                                ->scalarNode('role_admin')
                                    ->cannotBeEmpty()
                                    ->defaultValue(Ownable::DEFAULT_ROLEADMIN)
                                ->end()
                                ->booleanNode('nullable')
                                    ->defaultTrue()
                                ->end()
                                ->append($this->getFormNode())
                            ->end()
                        ->end()
                        // translatable behavior on specific fields
                        ->arrayNode('loggable')
                            ->children()
                                ->variableNode('whitelist') // logged fields (true = all)
                                    ->defaultValue(true)
                                ->end()
                                ->arrayNode('blacklist') // not logged fields
                                    ->scalarPrototype()->end()
                                ->end()
                                ->arrayNode('hidden') // fields not displayed in UI
                                    ->scalarPrototype()->end()
                                ->end()
                                ->arrayNode('embed') // assoc displayed
                                    ->scalarPrototype()->end()
                                ->end()
                            ->end()
                        ->end()
                        // translatable behavior on specific fields
                        ->arrayNode('translatable')
                            ->children()
                                ->arrayNode('fields')
                                    ->isRequired()
                                    ->cannotBeEmpty()
                                    ->scalarPrototype()->end()
                                ->end()
                            ->end()
                        ->end()
                        // sluggable behavior slug field
                        ->arrayNode('sluggable')
                            ->children()
                                ->scalarNode('separator')->end()
                                ->enumNode('style')
                                    ->defaultValue('default')
                                    ->values(['default', 'camel', 'lower', 'upper'])
                                ->end()
                                ->arrayNode('fields')
                                    ->isRequired()
                                    ->cannotBeEmpty()
                                    ->scalarPrototype()->end()
                                ->end()
                                ->booleanNode('updatable')
                                    ->defaultTrue()
                                ->end()
                                ->booleanNode('unique')
                                    ->defaultTrue()
                                ->end()
                                ->variableNode('form')
                                    ->defaultFalse()
                                ->end()
                                ->append($this->getValidatorsNode())
                            ->end()
                        ->end()
                        // sortable behavior position field
                        ->arrayNode('sortable')
                            ->children()
                                ->booleanNode('form')
                                    ->defaultTrue()
                                ->end()
                                ->arrayNode('grouping')
                                    ->scalarPrototype()
                                        ->isRequired()
                                        ->cannotBeEmpty()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                        // settingsable behavior
                        ->arrayNode('settingsable')
                            ->validate()
                                ->always()
                                ->then(function ($settingsable) {
                                    try {
                                        $processor = new Processor();
                                        $config = $processor->processConfiguration(
                                            (new SettingsableConfiguration()),
                                            [$settingsable['config'] ?? []]
                                        );

                                        $settingsable['config'] = $config;
                                    } catch (ConfigException $e) {
                                        $class = \get_class($e);
                                        $message = \sprintf(
                                            "Invalid settingsable config error:\n%s",
                                            $e->getMessage()
                                        );
                                        throw new $class($message);
                                    }

                                    return $settingsable;
                                })
                            ->end()
                            ->children()
                                ->arrayNode('config')
                                    ->variablePrototype()->end()
                                ->end()
                            ->end()
                        ->end()
                        // onerow behavior
                        ->arrayNode('onerow')
                            ->children()
                                ->booleanNode('instantiate')
                                    ->defaultFalse()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                // identifiers
                ->arrayNode('identifiers')
                    ->arrayPrototype()
                        ->children()
                            ->enumNode('type')
                                ->cannotBeEmpty()
                                ->defaultValue('integer')
                                ->values(['integer', 'string'])
                            ->end()
                            ->arrayNode('generator')
                                ->addDefaultsIfNotSet()
                                ->children()
                                    ->enumNode('strategy')
                                        ->info('see ' . ORM\GeneratedValue::class)
                                        ->cannotBeEmpty()
                                        ->defaultValue('AUTO')
                                        ->values(['AUTO', 'SEQUENCE', 'TABLE', 'IDENTITY', 'NONE', 'UUID', 'CUSTOM'])
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                // fields
                ->append($this->getFieldNode())
                // relationships
                ->append($this->getRelationshipNode(self::ONE_TO_ONE))
                ->append($this->getRelationshipNode(self::ONE_TO_MANY))
                ->append($this->getRelationshipNode(self::MANY_TO_ONE))
                ->append($this->getRelationshipNode(self::MANY_TO_MANY))
                // class
                ->arrayNode('class')
                    ->children()
                        ->scalarNode('extends')->end()
                        ->arrayNode('implements')
                            ->scalarPrototype()->end()
                        ->end()
                    ->end()
                ->end()
                // class validators
                ->append($this->getValidatorsNode())
                // api platform
                ->variableNode('api')->end()
                // routing
                ->arrayNode('routing')
                    ->children()
                        ->arrayNode('actions')
                            ->append($this->getActionNode(
                                Action::LIST,
                                [
                                    'icon_class' => 'fas fa-bars',
                                    'route_methods' => $methodGet = [Request::METHOD_GET],
                                    'route_requirements' => [], // this is a list so no route_requirements
                                ]
                            ))
                            ->append($this->getActionNode(
                                Action::NEW,
                                [
                                    'icon_class' => 'fas fa-plus-circle text-' . Action::NEW->getColor(),
                                    'route_methods' => $methodsGetPost = [Request::METHOD_GET, Request::METHOD_POST],
                                ]
                            ))
                            ->append($this->getActionNode(
                                Action::SHOW,
                                [
                                    'icon_class' => 'fas fa-eye text-' . Action::SHOW->getColor(),
                                    'route_methods' => $methodGet,
                                ]
                            ))
                            ->append($this->getActionNode(
                                Action::EDIT,
                                [
                                    'icon_class' => 'fas fa-edit text-' . Action::EDIT->getColor(),
                                    'route_methods' => $methodsGetPost,
                                ]
                            ))
                            ->append($this->getActionNode(
                                Action::DUPLICATE,
                                [
                                    'icon_class' => 'fas fa-clone text-' . Action::DUPLICATE->getColor(),
                                    'route_methods' => $methodsGetPost,
                                ]
                            ))
                            ->append($this->getActionNode(
                                Action::DELETE,
                                [
                                    'icon_class' => 'fas fa-trash-alt text-' . Action::DELETE->getColor(),
                                    'route_methods' => $methodsGetPost,
                                ]
                            ))
                            ->append($this->getActionNode(
                                Action::TRASH,
                                [
                                    'icon_class' => 'fas fa-bars text-' . Action::TRASH->getColor(),
                                    'route_methods' => $methodGet,
                                    'route_requirements' => [], // this is a list so no route_requirements
                                ]
                            ))
                            ->append($this->getActionNode(
                                Action::RECOVER,
                                [
                                    'icon_class' => 'fas fa-share-square text-' . Action::RECOVER->getColor(),
                                    'route_methods' => $methodsGetPost,
                                ]
                            ))
                        ->end()
                        ->arrayNode('custom')
                            ->arrayPrototype()
                                ->canBeEnabled()
                                ->children()
                                    ->enumNode('permission')
                                        ->values(Permission::cases())
                                    ->end()
                                    ->arrayNode('route')
                                        ->addDefaultsIfNotSet()
                                        ->beforeNormalization()
                                            ->ifTrue(function ($v) {
                                                return is_string($v);
                                            })
                                            ->then(function ($v) {
                                                return [
                                                    'name' => $v,
                                                ];
                                            })
                                        ->end()
                                        ->children()
                                            ->scalarNode('name')
                                                ->isRequired()
                                                ->cannotBeEmpty()
                                            ->end()
                                            ->scalarNode('identifier')
                                                ->defaultValue('id')
                                            ->end()
                                            ->arrayNode('extra_params')
                                                ->scalarPrototype()->end()
                                            ->end()
                                        ->end()
                                    ->end()
                                    ->arrayNode('icon')
                                        ->isRequired()
                                        ->children()
                                            ->scalarNode('class')
                                                ->isRequired()
                                                ->cannotBeEmpty()
                                            ->end()
                                            ->scalarNode('disabled_condition')
                                                ->defaultValue('')
                                            ->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                // data
                ->arrayNode('data')
                    ->children()
                        ->integerNode('order')->end()
                        ->arrayNode('envs')
                            ->requiresAtLeastOneElement()
                            ->defaultValue(['dev', 'test'])
                            ->enumPrototype()
                                ->values(['dev', 'test', 'prod'])
                            ->end()
                        ->end()
                        ->arrayNode('fixtures')
                            ->arrayPrototype()
                                ->variablePrototype()->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }

    private function getFieldNode(): Builder\ArrayNodeDefinition
    {
        $treeBuilder = new Builder\TreeBuilder('fields');
        /** @var Builder\ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        $rClass = new \ReflectionClass(Types::class);
        $types = array_values($rClass->getConstants());

        $rootNode
            ->arrayPrototype()
                ->children()
                    // name
                    ->scalarNode('name')->end()
                    // field type
                    ->enumNode('type')
                        ->info('see' . Types::class)
                        ->values($types)
                    ->end()
                    // enum
                    ->scalarNode('enum')
                        ->defaultNull()
                    ->end()
                    // unique
                    ->booleanNode('unique')
                        ->defaultFalse()
                    ->end()
                    // nullable
                    ->booleanNode('nullable')
                        ->defaultFalse()
                    ->end()
                    // nullable
                    ->integerNode('length')->end()
                    // nullable
                    ->scalarNode('column_definition')
                        ->defaultNull()
                    ->end()
                    // field can be updated in ajax
                    ->booleanNode('xml_http_update')
                        ->defaultFalse()
                    ->end()
                    // validators
                    ->append($this->getValidatorsNode())
                    // options
                    ->arrayNode('options')
                        ->children()
                            ->scalarNode('default')->end()
                            ->scalarNode('comment')->end()
                            ->scalarNode('collation')->end()
                            ->booleanNode('unsigned')->end()
                            ->booleanNode('fixed')->end()
                        ->end()
                    ->end()
                    // serialization
                    ->append($this->getSerializationNode())
                    // form
                    ->append($this->getFormNode())
            ->end();

        return $rootNode;
    }

    private function getRelationshipNode(string $name): Builder\ArrayNodeDefinition
    {
        $treeBuilder = new Builder\TreeBuilder($name);
        /** @var Builder\ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        $children = $rootNode->arrayPrototype()
            ->validate()
                ->ifTrue(function ($data): bool {
                    return isset($data['cascade']) && !$data['cascade'];
                })
                ->then(function ($data): array {
                    unset($data['cascade']);

                    return $data;
                })
                ->end()
            ->children();

        // targetEntity
        $children->scalarNode('targetEntity')
            ->isRequired()
            ->cannotBeEmpty()
        ->end();
        // cascade
        $children->arrayNode('cascade')
            ->enumPrototype()
                ->values(['persist', 'remove', 'merge', 'detach', 'refresh', 'all'])
            ->end()
        ->end();
        // fetch
        $children->enumNode('fetch')
            ->values(['LAZY', 'EAGER', 'EXTRA_LAZY'])
        ->end();

        if (self::MANY_TO_ONE !== $name) {
            // mappedBy
            $children->scalarNode('mappedBy')->end();
        }

        if (self::ONE_TO_MANY !== $name) {
            // inversedBy
            $children->scalarNode('inversedBy')->end();
        }

        if (in_array($name, [self::ONE_TO_ONE, self::ONE_TO_MANY])) {
            // orphanRemoval
            $children->booleanNode('orphanRemoval')->end();
        }

        if (in_array($name, [self::ONE_TO_MANY, self::MANY_TO_MANY])) {
            // orderby
            $children->arrayNode('orderBy')
                ->enumPrototype()
                    ->values(['ASC', 'DESC'])
                ->end()
            ->end();
        }

        if (self::ONE_TO_ONE !== $name) {
            // indexBy
            $children->scalarNode('indexBy')->end();
        }

        if (in_array($name, [self::ONE_TO_ONE, self::MANY_TO_ONE])) {
            // join column
            $children->arrayNode('joinColumn')
                ->children()
                    ->scalarNode('name')->end()
                    ->scalarNode('referencedColumnName')->end()
                    ->booleanNode('unique')->end()
                    ->booleanNode('nullable')->end()
                    ->variableNode('onDelete')->end()
                    ->variableNode('columnDefinition')->end()
                    ->variableNode('fieldName')->end()
                    ->arrayNode('options')
                        ->variablePrototype()->end()
                    ->end()
                ->end()
            ->end();
        }

        if (self::MANY_TO_MANY === $name) {
            $children->arrayNode('joinTable')
                ->children()
                    ->scalarNode('name')->end()
                    ->scalarNode('schema')->end()
                    ->arrayNode('joinColumns')
                        ->arrayPrototype()
                            ->children()
                                ->scalarNode('name')->end()
                                ->scalarNode('referencedColumnName')->end()
                                ->booleanNode('unique')->end()
                                ->booleanNode('nullable')->end()
                                ->variableNode('onDelete')->end()
                                ->variableNode('columnDefinition')->end()
                                ->variableNode('fieldName')->end()
                                ->arrayNode('options')
                                    ->variablePrototype()->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                    ->arrayNode('inverseJoinColumns')
                        ->arrayPrototype()
                            ->children()
                                ->scalarNode('name')->end()
                                ->scalarNode('referencedColumnName')->end()
                                ->booleanNode('unique')->end()
                                ->booleanNode('nullable')->end()
                                ->variableNode('onDelete')->end()
                                ->variableNode('columnDefinition')->end()
                                ->variableNode('fieldName')->end()
                                ->arrayNode('options')
                                    ->variablePrototype()->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                    ->variableNode('options')->end()
                    ->end()
                ->end()
            ->end();
        }

        // serialization
        $children->append($this->getSerializationNode());
        // form
        $children->append($this->getFormNode());
        // validators
        $children->append($this->getValidatorsNode());

        $children->end()->end();

        return $rootNode;
    }

    private function getActionNode(Action $action, array $defaults): Builder\ArrayNodeDefinition
    {
        $resolver = new OptionsResolver();
        // icon_class
        $resolver->setRequired('icon_class');
        $resolver->setAllowedTypes('icon_class', 'string');
        // route_methods
        $resolver->setRequired('route_methods');
        $resolver->setAllowedTypes('route_methods', 'array');
        // route_requirements
        $resolver->setDefault('route_requirements', ['id' => '\d+']);
        $resolver->setAllowedTypes('route_requirements', 'array');
        // route_requirements
        $resolver->setDefault('route_defaults', []);
        $resolver->setAllowedTypes('route_defaults', 'array');

        $defaults = $resolver->resolve($defaults);

        $treeBuilder = new Builder\TreeBuilder($action->value);
        /** @var Builder\ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        $defaults['color'] = $action->getColor();

        /** @var Builder\NodeBuilder $node */
        $node = $rootNode
            ->canBeEnabled()
            ->children()
                ->enumNode('permission')
                    ->values(Permission::cases())
                ->end()
                ->arrayNode('route')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('path')
                            ->defaultNull()
                        ->end()
                        ->arrayNode('methods')
                            ->defaultValue($defaults['route_methods'])
                            ->scalarPrototype()->end()
                        ->end()
                        ->scalarNode('condition')
                            ->defaultValue('')
                        ->end()
                        ->arrayNode('requirements')
                            ->defaultValue($defaults['route_requirements'])
                            ->scalarPrototype()->end()
                        ->end()
                        ->arrayNode('defaults')
                            ->defaultValue($defaults['route_defaults'])
                            ->scalarPrototype()->end()
                        ->end()
                        ->scalarNode('identifier')
                            ->defaultValue('id')
                        ->end()
                        ->arrayNode('extra_params')
                            ->scalarPrototype()->end()
                        ->end()
                    ->end()
                ->end()
                ->enumNode('color')
                    ->values(Action::getColors())
                    ->defaultValue($defaults['color'])
                ->end()
                ->arrayNode('icon')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('class')
                            ->defaultValue($defaults['icon_class'])
                        ->end()
                        ->scalarNode('disabled_condition')
                            ->defaultValue('')
                        ->end()
                    ->end()
                ->end()
                ->scalarNode('title')
                    ->defaultValue('entity_name_plural ~ " " ~ action')
                ->end();

        if ($action->isCrud(true)) {
            $methodEnabled = $action->hasForm() ? 'canBeEnabled' : 'canBeDisabled';
            $node
                ->arrayNode('modal')
                    ->$methodEnabled()
                    ->children()
                        ->scalarNode('form_redirect_success')
                            ->defaultNull()
                        ->end()
                        ->enumNode('color')
                            ->values(array_merge([null], Action::getColors()))
                            ->defaultValue(null)
                        ->end()
                        ->enumNode('size')
                            ->values(['sm', 'md', 'lg'])
                            ->defaultValue('md')
                        ->end()
                    ->end()
                ->end();
        }

        // specific action config
        switch ($action) {
            case Action::LIST:
            case Action::TRASH:
                $node
                    ->arrayNode('table')
                        ->children()
                            ->arrayNode('ajax')
                                ->canBeEnabled()
                            ->end()
                            // # Required. List table columns
                            ->arrayNode('columns')
                                ->cannotBeEmpty()
                                ->requiresAtLeastOneElement()
                                ->arrayPrototype()
                                    // twig 'data_type' to 'twig.template'
                                    ->beforeNormalization()
                                        ->ifTrue(function ($v) {
                                            return isset($v['data_type']) && $v['data_type'];
                                        })
                                        ->then(function ($v) {
                                            $v['twig']['template'] = "_datatypes/_{$v['data_type']}.html.twig";
                                            unset($v['data_type']);

                                            return $v;
                                        })
                                    ->end()
                                    ->children()
                                        // langugage expression to get cell value. Default is the field name.
                                        ->scalarNode('expression')->end()
                                        // Column display condition
                                        // by request, route_name, loggedin_user, user_is_admin)
                                        ->scalarNode('condition')->end()
                                        // Hidden values for extends table search
                                        ->scalarNode('search_fields')->end()
                                        // Default column order
                                        ->enumNode('order')->values(['asc', 'desc'])->end()
                                        // Override Twig template. (data_type: 'boolean' or 'array' etc...)
                                        ->scalarNode('data_type')->end()
                                        ->arrayNode('twig')
                                            ->children()
                                                ->scalarNode('template')->end()
                                                ->arrayNode('variables')
                                                    ->variablePrototype()->end()
                                                ->end()
                                            ->end()
                                        ->end()
                                        // attr for each table cell
                                        ->arrayNode('cell_attr')
                                            ->scalarPrototype()->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end();
                break;
            case Action::DUPLICATE:
                $node
                    ->arrayNode('cloning')
                        ->variablePrototype()->end()
                    ->end();
                break;
        }

        $rootNode->end();

        return $rootNode;
    }

    private function getFormNode(bool $enabled = true): Builder\ArrayNodeDefinition
    {
        $treeBuilder = new Builder\TreeBuilder('form');
        /** @var Builder\ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        $method = $enabled ? 'canBeDisabled' : 'canBeEnabled';
        $node = $rootNode
            ->$method()
            // validate get-repo/formyaml config
            ->validate()
                ->always()
                ->then(function ($formConfig) {
                    try {
                        $processor = new Processor();
                        $formConfig = $processor->processConfiguration(
                            (new FormConfiguration()),
                            [['field' => $formConfig]]
                        );

                        $formConfig = $formConfig['field'];
                    } catch (ConfigException $e) {
                        $class = \get_class($e);
                        $message = \sprintf(
                            "Invalid form field config error:\n%s",
                            \str_replace('form_definitions.', '', $e->getMessage())
                        );
                        throw new $class($message);
                    }

                    return $formConfig;
                })
            ->end()
            ->ignoreExtraKeys(false)
            ->children()->end();

        return $node;
    }

    private function getValidatorsNode(): Builder\ArrayNodeDefinition
    {
        $treeBuilder = new Builder\TreeBuilder('validators');
        /** @var Builder\ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->beforeNormalization()
                ->always()
                ->then(function (array $constraints): array {
                    return $this->resolver->resolveConstraints($constraints);
                })
            ->end()
            ->variablePrototype()->end(); // constraint object

        return $rootNode;
    }

    private function getSerializationNode(): Builder\ArrayNodeDefinition
    {
        $treeBuilder = new Builder\TreeBuilder('serialization');
        /** @var Builder\ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
                ->arrayNode('groups')
                    ->variablePrototype()
                        ->cannotBeEmpty()
                    ->end()
                ->end()
            ->end();

        return $rootNode;
    }
}
