<?php

namespace GetRepo\Generator\Configuration;

use Symfony\Component\Config\Definition\Builder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class ControllerConfiguration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): Builder\TreeBuilder
    {
        // empty so far
        return new Builder\TreeBuilder('controller_mapping');
    }
}
