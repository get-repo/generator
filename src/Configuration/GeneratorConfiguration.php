<?php

namespace GetRepo\Generator\Configuration;

use GetRepo\Generator\DependencyInjection\GetRepoGeneratorExtension;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\EnumNodeDefinition;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

final class GeneratorConfiguration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder(GetRepoGeneratorExtension::ALIAS);
        /** @var \Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
                ->arrayNode('behaviors')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->arrayNode('ownable')
                            ->children()
                                ->scalarNode('user_class')
                                    ->isRequired()
                                    ->cannotBeEmpty()
                                ->end()
                            ->end()
                        ->end()
                        ->arrayNode('workflow')
                            ->children()
                                ->scalarNode('form_type')
                                    ->cannotBeEmpty()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->append($this->addEntityNode())
                ->append($this->addControllerNode())
            ->end();

        return $treeBuilder;
    }

    private function addEntityNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('entity');

        $inheritance = new ArrayNodeDefinition('inheritance');
        $inheritance->arrayPrototype()
            ->addDefaultsIfNotSet()
            ->children()
                ->scalarNode('prefix')
                    ->cannotBeEmpty()
                    ->defaultValue('')
                ->end()
            ->end()
        ->end();

        $node = $treeBuilder->getRootNode()
            ->validate()
                ->ifTrue(function ($data) {
                    return !array_key_exists($data['default'], $data['profiles']);
                })
                ->thenInvalid('Default profile does not exists %s')
            ->end()
            ->children()
                ->scalarNode('default')
                    ->cannotBeEmpty()
                    ->defaultValue('_default')
                ->end()
                ->arrayNode('profiles')
                    ->isRequired()
                    ->requiresAtLeastOneElement()
                    ->arrayPrototype()
                        ->addDefaultsIfNotSet()
                        ->children()
                            ->scalarNode('namespace')
                                ->isRequired()
                                ->cannotBeEmpty()
                            ->end()
                            ->scalarNode('prefix')
                                ->cannotBeEmpty()
                                ->defaultNull()
                            ->end()
                            ->arrayNode('implements')
                                ->scalarPrototype()
                                    ->isRequired()
                                    ->cannotBeEmpty()
                                ->end()
                            ->end()
                            ->booleanNode('identifiers')
                                ->defaultFalse()
                            ->end()
                            ->arrayNode('methods')
                                ->addDefaultsIfNotSet()
                                ->children()
                                    ->booleanNode('auto_getters')
                                        ->defaultFalse()
                                    ->end()
                                    ->booleanNode('auto_setters')
                                        ->defaultFalse()
                                    ->end()
                                    ->booleanNode('auto_sorting')
                                        ->defaultTrue()
                                    ->end()
                                    ->arrayNode('magic')
                                        ->children()
                                            ->scalarNode('to_string')
                                                ->cannotBeEmpty()
                                                ->defaultNull()
                                            ->end()
                                            ->booleanNode('auto_clone')
                                                ->defaultFalse()
                                            ->end()
                                            ->scalarNode('clone')
                                                ->cannotBeEmpty()
                                                ->defaultNull()
                                            ->end()
                                        ->end()
                                    ->end()
                                    ->arrayNode('custom')
                                        ->useAttributeAsKey('name')
                                        ->arrayPrototype()
                                            ->children()
                                                // TODO implement method parameters
                                                ->booleanNode('abstract')
                                                    ->defaultFalse()
                                                ->end()
                                                ->enumNode('visibility')
                                                    ->isRequired()
                                                    ->values(['public', 'protected', 'private'])
                                                ->end()
                                                ->scalarNode('return_type')
                                                    ->isRequired()
                                                    ->cannotBeEmpty()
                                                ->end()
                                                ->scalarNode('body')
                                                    ->cannotBeEmpty()
                                                ->end()
                                            ->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                            ->arrayNode('constructor')
                                ->children()
                                    ->scalarNode('body')
                                        ->cannotBeEmpty()
                                    ->end()
                                ->end()
                            ->end()
                            ->arrayNode('properties')
                                ->children()
                                    ->enumNode('define_as')
                                        ->values([
                                            'property',
                                            'constructor_property_promotion',
                                            'property_with_constructor_args',
                                        ])
                                        ->defaultValue('property')
                                    ->end()
                                    ->append($this->addVisibilityNode())
                                ->end()
                            ->end()
                            ->arrayNode('comments')
                                ->children()
                                    ->arrayNode('class')
                                        ->scalarPrototype()
                                            ->cannotBeEmpty()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                            ->append($inheritance)
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $node;
    }

    private function addControllerNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('controller');

        $inheritance = new ArrayNodeDefinition('inheritance');
        $inheritance->arrayPrototype()
            ->addDefaultsIfNotSet()
            ->children()
                ->scalarNode('prefix')
                    ->cannotBeEmpty()
                    ->defaultValue('')
                ->end()
            ->end()
        ->end();

        $node = $treeBuilder->getRootNode()
            ->validate()
                ->ifTrue(function ($data) {
                    return !array_key_exists($data['default'], $data['profiles']);
                })
                ->thenInvalid('Default profile does not exists %s')
            ->end()
            ->children()
                ->scalarNode('default')
                    ->cannotBeEmpty()
                    ->defaultValue('_default')
                ->end()
                ->arrayNode('profiles')
                    ->isRequired()
                    ->requiresAtLeastOneElement()
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('namespace')
                                ->isRequired()
                                ->cannotBeEmpty()
                            ->end()
                            ->scalarNode('prefix')
                                ->cannotBeEmpty()
                                ->defaultNull()
                            ->end()
                            ->scalarNode('extends')
                                ->cannotBeEmpty()
                                ->defaultNull()
                            ->end()
                            ->append($inheritance)
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $node;
    }

    private function addVisibilityNode(): NodeDefinition
    {
        $node = new EnumNodeDefinition('visibility');

        $node
            ->defaultValue('protected')
            ->values(['public', 'protected', 'private'])
        ;

        return $node;
    }
}
