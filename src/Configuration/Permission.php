<?php

namespace GetRepo\Generator\Configuration;

enum Permission: string
{
    case PUBLIC_ACCESS = 'PUBLIC_ACCESS';
    case ANONYMOUSLY_ONLY = 'ANONYMOUSLY_ONLY';
    case ROLE_USER = 'ROLE_USER';
    case ROLE_ADMIN = 'ROLE_ADMIN';

    public function isLoggedIn(): bool
    {
        return \in_array($this, [self::ROLE_USER, self::ROLE_ADMIN], true);
    }
}
