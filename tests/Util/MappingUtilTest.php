<?php

namespace Tests\Util;

use Doctrine\ORM\Mapping\Column;
use GetRepo\Generator\Util\MappingUtil;
use PHPUnit\Framework\TestCase;

class MappingUtilTest extends TestCase
{
    public static function hasFieldOrAssocProvider(): array
    {
        return [
            'empty mapping' => [false, []],
            'mapping has unexpected fields' => [false, ['unexpected' => 1]],
            'mapping has empty fields' => [false, [MappingUtil::FIELDS => []]],
            'mapping has fields' => [true, [MappingUtil::FIELDS => ['test' => null]]],
            'mapping has empty one to one' => [false, [MappingUtil::ONE_TO_ONE => []]],
            'mapping has empty one to many' => [false, [MappingUtil::ONE_TO_MANY => []]],
            'mapping has empty many to one' => [false, [MappingUtil::MANY_TO_ONE => []]],
            'mapping has empty many to many' => [false, [MappingUtil::MANY_TO_MANY => []]],
            'mapping has one to one' => [true, [MappingUtil::ONE_TO_ONE => ['test' => []]]],
            'mapping has one to many' => [true, [MappingUtil::ONE_TO_MANY => ['test' => []]]],
            'mapping has many to one' => [true, [MappingUtil::MANY_TO_ONE => ['test' => []]]],
            'mapping has many to many' => [true, [MappingUtil::MANY_TO_MANY => ['test' => []]]],
            'mapping has fields and relationships' => [
                true,
                [MappingUtil::FIELDS => ['test' => null], MappingUtil::ONE_TO_ONE => ['test2' => []]],
            ],
        ];
    }

    /**
     * @dataProvider hasFieldOrAssocProvider
     */
    public function testHasFieldOrAssoc(bool $expected, array $mapping): void
    {
        $this->assertEquals($expected, MappingUtil::hasFieldOrAssoc($mapping));
    }

    public static function getAllFieldsAndAssocsProvider(): array
    {
        return [
            'empty mapping' => [[], []],
            'mapping has unexpected fields' => [[], ['unexpected' => 1]],
            'mapping has empty fields' => [[], [MappingUtil::FIELDS => []]],
            'mapping has fields' => [
                ['test' => ['type' => 'string']],
                [MappingUtil::FIELDS => ['test' => ['type' => 'string']]],
            ],
            'mapping has fields and relationships' => [
                [
                    'test' => ['type' => 'string'],
                    'user' => [],
                    'group' => [],
                ],
                [
                    MappingUtil::FIELDS => ['test' => ['type' => 'string']],
                    MappingUtil::ONE_TO_ONE => ['user' => []],
                    MappingUtil::MANY_TO_ONE => ['group' => []],
                ],
            ],
            'mapping sorting' => [
                [
                    'item1' => [],
                    'item2' => [],
                    'item3' => [],
                    // TODO get we also sort optional relationship at the end ? #sort-optional-assoc-end
                    'item6' => ['nullable' => true],
                    'item4' => ['nullable' => true],
                    'item5' => ['options' => ['default' => 'test']],
                ],
                [
                    MappingUtil::MANY_TO_ONE => ['item3' => []],
                    MappingUtil::FIELDS => [
                        'item4' => ['nullable' => true],
                        'item1' => [],
                        'item5' => ['options' => ['default' => 'test']],
                        'item2' => [],
                    ],
                    MappingUtil::ONE_TO_ONE => ['item6' => ['nullable' => true]],
                ],
            ],
        ];
    }

    /**
     * @dataProvider getAllFieldsAndAssocsProvider
     */
    public function testGetAllFieldsAndAssocs(array $expected, array $mapping): void
    {
        $this->assertSame($expected, MappingUtil::getAllFieldsAndAssocs($mapping));
    }

    public static function getAllFieldsProvider(): array
    {
        return [
            'empty mapping' => [[], []],
            'mapping has unexpected fields' => [[], ['unexpected' => 1]],
            'mapping has empty fields' => [[], [MappingUtil::FIELDS => []]],
            'mapping sorting' => [
                [
                    'item1' => [],
                    'item2' => [],
                    'item3' => ['options' => ['default' => 'test']],
                    'item4' => ['nullable' => true],
                ],
                [
                    MappingUtil::FIELDS => [
                        'item4' => ['nullable' => true],
                        'item1' => [],
                        'item3' => ['options' => ['default' => 'test']],
                        'item2' => [],
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider getAllFieldsProvider
     */
    public function testGetAllFields(array $expected, array $mapping): void
    {
        $this->assertSame($expected, MappingUtil::getAllFields($mapping));
    }

    public static function getAllAssocsProvider(): array
    {
        return [
            'empty mapping' => [[], []],
            'mapping has unexpected fields' => [[], ['unexpected' => 1]],
            'mapping has empty fields' => [[], [MappingUtil::FIELDS => []]],
            'mapping has fields' => [
                [],
                [MappingUtil::FIELDS => ['test' => []]],
            ],
            'mapping has fields and relationships' => [
                [
                    'user' => [],
                    'group' => [],
                ],
                [
                    MappingUtil::FIELDS => ['test' => ['type' => 'string']],
                    MappingUtil::ONE_TO_ONE => ['user' => []],
                    MappingUtil::MANY_TO_ONE => ['group' => []],
                ],
            ],
            'mapping sorting' => [
                [
                    'item1' => [],
                    'item2' => ['nullable' => true],
                ],
                [
                    MappingUtil::MANY_TO_ONE => ['item1' => []],
                    MappingUtil::FIELDS => [
                        'item3' => [],
                    ],
                    MappingUtil::ONE_TO_ONE => ['item2' => ['nullable' => true]],
                ],
            ],
        ];
    }

    /**
     * @dataProvider getAllAssocsProvider
     */
    public function testGetAllAssocs(array $expected, array $mapping): void
    {
        $this->assertSame($expected, MappingUtil::getAllAssocs($mapping));
    }

    public static function getPropertyMatchesProvider(): array
    {
        return [
            'Column empty field options' => [[], []],
            'Column field options has unexpected fields' => [[], ['unexpected' => 1]],
            'Column mapping valid, nullable default value and unexpected' => [
                ['name' => 'test'],
                ['name' => 'test', 'unexpected' => 2, 'nullable' => false],
            ],
            'Column mapping valid' => [
                ['name' => 'test', 'scale' => 45, 'unique' => true],
                ['name' => 'test', 'scale' => 45, 'unique' => true],
            ],
        ];
    }

    /**
     * @dataProvider getPropertyMatchesProvider
     */
    public function testGetPropertyMatches(array $expected, array $fieldMapping): void
    {
        $this->assertSame($expected, MappingUtil::getPropertyMatches($fieldMapping, Column::class));
    }

    public static function getTypeHintProvider(): array
    {
        return [
            'type null' => ['string', null],
            'type whatever' => ['string', 'whatever'],
            'type boolean' => ['bool', 'boolean'],
            'type integer' => ['int', 'integer'],
            'type bigint' => ['int', 'bigint'],
            'type float' => ['float', 'float'],
            'type decimal' => ['float', 'decimal'],
            'type simple array' => ['array', 'simple_array'],
            'type json' => ['array', 'json'],
            'type array' => ['array', 'array'],
            'type date' => [\DateTime::class, 'date'],
            'type datetime' => [\DateTime::class, 'datetime'],
            'type date immutable' => [\DateTimeImmutable::class, 'date_immutable'],
            'type datetime immutable' => [\DateTimeImmutable::class, 'datetime_immutable'],
        ];
    }

    /**
     * @dataProvider getTypeHintProvider
     */
    public function testGetTypeHint(string $expected, ?string $type): void
    {
        $this->assertSame($expected, MappingUtil::getTypeHint($type));
    }

    public static function getAssocClassProvider(): array
    {
        return [
            'one to one' => ['Doctrine\ORM\Mapping\OneToOne', 'oneToOne'],
            'one to many' => ['Doctrine\ORM\Mapping\OneToMany', 'oneToMany'],
            'many to one' => ['Doctrine\ORM\Mapping\ManyToOne', 'manyToOne'],
            'many to many' => ['Doctrine\ORM\Mapping\ManyToMany', 'manyToMany'],
        ];
    }

    /**
     * @dataProvider getAssocClassProvider
     */
    public function testGetAssocClass(string $expected, ?string $type): void
    {
        $this->assertSame($expected, MappingUtil::getAssocClass($type));
    }
}
