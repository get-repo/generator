<?php

namespace Tests;

use GetRepo\Generator\ControllerGenerator;
use GetRepo\Generator\DependencyInjection\GetRepoGeneratorExtension;
use GetRepo\Generator\EntityGenerator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Yaml;

class GeneratorTest extends TestCase
{
    public const FILE_EXT = '.test.yml';

    public static function entityProvider(): array
    {
        return self::buildProvider(directory: 'entity');
    }

    /**
     * @dataProvider entityProvider
     */
    public function testEntityGenerator(array $profile, array $entity, array $options, array $expected): void
    {
        $container = new ContainerBuilder();
        $container->registerExtension(new GetRepoGeneratorExtension());
        $container->loadFromExtension(
            GetRepoGeneratorExtension::ALIAS,
            [
                'behaviors' => [
                    'ownable' => ['user_class' => 'NameSpace\Entity\User'],
                ],
                'entity' => [
                    'default' => 'test',
                    'profiles' => ['test' => $profile],
                ],
            ]
        );
        $container->compile();
        /** @var EntityGenerator $generator */
        $generator = $container->get(EntityGenerator::class);
        $result = $generator->generate(
            shortName: 'MyTest',
            mapping: array_merge(['name' => 'App\Test', 'table' => 'my_test'], $entity),
            options: array_merge($options, ['profile' => 'test']),
        );
        $this->assertIsArray($result);
        $this->assertEquals($result, $expected);
    }

    public static function controllerProvider(): array
    {
        return self::buildProvider(directory: 'controller');
    }

    /**
     * @dataProvider controllerProvider
     */
    public function testControllerGenerator(array $profile, array $options, array $expected): void
    {
        $container = new ContainerBuilder();
        $container->registerExtension(new GetRepoGeneratorExtension());
        $container->loadFromExtension(
            GetRepoGeneratorExtension::ALIAS,
            [
                'controller' => [
                    'default' => 'test',
                    'profiles' => ['test' => $profile],
                ],
            ]
        );
        $container->compile();
        /** @var EntityGenerator $generator */
        $generator = $container->get(ControllerGenerator::class);
        $result = $generator->generate(
            shortName: 'MyTest',
            mapping: [],
            options: array_merge($options, ['profile' => 'test']),
        );
        $this->assertIsArray($result);
        $this->assertEquals($result, $expected);
    }

    private static function buildProvider(string $directory): array
    {
        $finder = new Finder();
        $finder->files()
            ->in(sprintf('%s/%s', __DIR__, $directory))
            ->name('*' . self::FILE_EXT);
        $tests = [];
        /** @var \Symfony\Component\Finder\SplFileInfo $file */
        foreach ($finder as $file) {
            $data = Yaml::parseFile(
                $file->getRealPath(),
                Yaml::PARSE_DATETIME | Yaml::PARSE_CUSTOM_TAGS | Yaml::PARSE_CONSTANT,
            );
            foreach ($data as $name => $test) {
                $tests[sprintf('%s %s : %s', ucfirst($directory), $file->getBasename(self::FILE_EXT), $name)] = $test;
            }
        }

        return $tests;
    }
}
