<p align="center">
    <img src="https://gitlab.com/uploads/-/system/project/avatar/52004404/generator_logo.png" height=100 />
</p>

<h1 align=center>Generator</h1>

<br/>

Generator is a tool to generate Symfony Doctrine PHP entities based on YAML definitions.

## Table of Contents

1. [Installation](#installation)
1. [Example](#example)

<br/>

## Installation

This is installable via [Composer](https://getcomposer.org/):

    composer config repositories.get-repo/generator git https://gitlab.com/get-repo/generator.git
    composer require get-repo/generator

### Enable the bundle

In *config/bundles.php* if not already done by Symfony already
```php
<?php
return [
    ...
    GetRepo\Generator\GetRepoGeneratorBundle::class => [all' => true],
];
```

<br/>

## Configuration Reference

```yaml
# config/packages/generator.yaml

getrepo_generator:
    behaviors:
        ownable:
            field_name: owner
            user_class: 'App\Entity\User'
            nullable: true
        workflow:
            form_type: 'App\Type\Workflow'
    entity:
        default: 'profile_1'
        profiles:
            profile_1:
                namespace: 'My\Namespace\Entity\Base'
                prefix: 'Prefixed'
                implements: [My\Namespace\Entity\Base\SfyEntityInterface]
                identifiers: true
                constructor:
                    body: '// custom code'
                properties:
                    define_as: 'property_with_constructor_args' # property, constructor_property_promotion, property_with_constructor_args
                methods:
                    auto_getters: true
                    auto_setters: true
                    auto_sorting: true
                    magic:
                        to_string: "return 'TODO';"
                        auto_clone: true
                        clone: '// custom code'
                        custom:
                            customArray:
                                abstract: false
                                visibility: public
                                return_type: array
                                body: 'return []'
                comments:
                    class:
                        - 'phpcs:disable Generic.Files.LineLength.TooLong'
                inheritance:
                    'My\Namespace\Entity':
                        prefix: 'Core'
                    'App\Entity\Base':
                        prefix: 'Base'
                    'App\Entity': ~
```